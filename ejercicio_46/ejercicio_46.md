## Consigna:

Se requiere de un algoritmo que lea los 250.000 votos de los 3 candidatos a intendente de la ciudad. Luego informar el dato del candidato ganador y los porcentajes de este candidato y los demás. (Función Aleatorio devuelve un numero en un rango definido, en este caso del 1 al 3, Ejemplo: Aleatorio(1,3) así defino la función para el seudocódigo). **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***inicializar*** total_votos_candidato1, total_votos_candidato2, total_votos_candidato3 <-- 0
    - ***definir*** total_votos <-- 250000
    
    - ***repetir*** para cada voto en el rango desde 1 hasta total_votos:
        - ***llamar*** Aleatorio(1, 3) y guardar el resultado en numero_candidato
        
        - ***si*** numero_candidato es igual a 1:
            - ***incrementar*** total_votos_candidato1 en 1
        - ***sino si*** numero_candidato es igual a 2:
            - ***incrementar*** total_votos_candidato2 en 1
        - ***sino***:
            - ***incrementar*** total_votos_candidato3 en 1
    
    - ***calcular*** porcentaje_candidato1 <-- (total_votos_candidato1 / total_votos) * 100
    - ***calcular*** porcentaje_candidato2 <-- (total_votos_candidato2 / total_votos) * 100
    - ***calcular*** porcentaje_candidato3 <-- (total_votos_candidato3 / total_votos) * 100
    
    - ***si*** total_votos_candidato1 > total_votos_candidato2 y total_votos_candidato1 > total_votos_candidato3:
        - ***escribir*** "El candidato 1 es el ganador con", porcentaje_candidato1, "% de los votos."
    - ***sino si*** total_votos_candidato2 > total_votos_candidato1 y total_votos_candidato2 > total_votos_candidato3:
        - ***escribir*** "El candidato 2 es el ganador con", porcentaje_candidato2, "% de los votos."
    - ***sino***:
        - ***escribir*** "El candidato 3 es el ganador con", porcentaje_candidato3, "% de los votos."
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    
```