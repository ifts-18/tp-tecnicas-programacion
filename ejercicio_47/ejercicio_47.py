
notas_1 = [0] * 100
notas_2 = [0] * 100
prom = [0] * 100

for i in range(83):
    notas_1[i] = float(input(f"Ingrese la nota del primer parcial para el alumno: {i}"))
    notas_2[i] = float(input(f"Ingrese la nota del segundo parcial para el alumno: {i}"))

for i in range(83):
    prom[i] = (notas_1[i] + notas_2[i]) / 2

alumno = int(input("Ingrese un alumno para ver su promedio "))

print(f"El promedio del alumno {alumno} es: {prom[alumno]}")
