## Consigna:

Se requiere un algoritmo que calcule el sueldo neto de un trabajador. Para ello, el algoritmo debe admitir el ingreso del monto a cobrar por horas y el total de horas trabajadas. Si el empleado trabajo más de 160 horas mensuales se deben considerar la diferencia como horas extras y el monto por hora deberá ser el doble del valor ingresado en un inicio. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - **escribir** "Ingrese el monto a cobrar por hora:"
    - **leer** MONTO_POR_HORA
    - **escribir** "Ingrese el total de horas trabajadas:"
    - **leer** TOTAL_HORAS_TRABAJADAS
    - **hacer** HORAS_EXTRAS <-- máximo(TOTAL_HORAS_TRABAJADAS - 160, 0)
    - **hacer** VALOR_HORAS_EXTRA <-- MONTO_POR_HORA * 2
    - **hacer** SUELDO_NORMAL <-- MONTO_POR_HORA * 160
    - **hacer** SUELDO_TOTAL_EXTRA <-- SUELDO_NORMAL + (VALOR_HORAS_EXTRA * HORAS_EXTRAS)
    - **si** TOTAL_HORAS_TRABAJADAS > 160:
        - **escribir** "El sueldo total es:  SUELDO_TOTAL_EXTRA
    - **sino**:
        - **escribir** "El sueldo total es: $", SUELDO_NORMAL
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicio --> escribir[\"'Ingrese el monto a cobrar por hora:'"/]
    escribir --> leer1[/MONTO_POR_HORA/]
    leer1 --> escribir2[\"'Ingrese el total de horas trabajadas:'"/]
    escribir2 --> leer2[/TOTAL_HORAS_TRABAJADAS/]
    leer2 --> calcular1[HORAS_EXTRAS <-- TOTAL_HORAS_TRABAJADAS - 160]
    calcular1 --> calcular2[VALOR_HORAS_EXTRA = MONTO_POR_HORA * 2]
    calcular2 --> calcular3[SUELDO_NORMAL <-- MONTO_POR_HORA * 160]
    calcular3 --> calcular4[SUELDO_TOTAL_EXTRA <-- SUELDO_NORMAL + VALOR_HORAS_EXTRA * HORAS_EXTRAS]
    calcular4 --> si{TOTAL_HORAS_TRABAJADAS > 160}
    si --si --> escribir3[\"'El sueldo total es: SUELDO_TOTAL_EXTRA"/]
    escribir3 --> fin([FIN])
    si --no --> escribir4[\"'El sueldo total es: SUELDO_NORMAL"/]
    escribir4 --> fin
```