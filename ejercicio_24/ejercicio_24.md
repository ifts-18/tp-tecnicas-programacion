## Consigna:

Se requiere un algoritmo que calcule el costo de internación de un 
paciente. Para ello, se debe solicitar ingrese en que categoría se 
encuentra y la cantidad de días que se encuentra hospitalizado. 

Las categorías de internación son las siguientes:
- Pediatría
- Maternidad
- Otro

Y los costros de internación por especialidad son los siguientes:
- Pediatría: $2.500
- Maternidad: $3.500
- Otro: $3.000

### Pseudocódigo

- ***INICIO***
  - ***Hacer***  
    - COSTO_PEDIATRIA <-- 2500  
    - COSTO_MATERNIDAD <-- 3500  
    - COSTO_OTRO <-- 3000
  - ***Hacer***
    - CATEGORIA_ELEGIDA, como tipo caracter  
    - DIAS_INTERNACION, como tipo entero  
    - COSTO_INTERNACION, como tipo decimal
  - ***Escribir*** "Categorías: pediatria, maternidad, otro"
  - ***Escribir*** "Ingresar categoría: "
  - ***Leer*** CATEGORIA_ELEGIDA
  - ***Escribir*** "Ingresar días de internación: "
  - ***Leer*** DIAS_INTERNACION
  - ***Si*** CATEGORIA_ELEGIDA = 'pediatria'
    - ***entonces***
      - ***Hacer*** COSTO_INTERNACION <-- COSTO_PEDIATRIA * DIAS_INTERNACION
      - ***Escribir*** "Costo de internación", COSTO_INTERNACION
    - ***sino***
      - ***Si*** CATEGORIA_ELEGIDA = 'maternidad'
        - ***entonces***
          - ***Hacer*** COSTO_INTERNACION <-- COSTO_MATERNIDAD * DIAS_INTERNACION
          - ***Escribir*** "Costo de internación", COSTO_INTERNACION
        - ***sino*** COSTO_INTERNACION <-- COSTO_OTRO * DIAS_INTERNACION
        - ***Escribir*** "Costo de internación", COSTO_INTERNACION
- ***FIN***


### Diagrama de Flujo

```mermaid
flowchart TD
    INICIO([INICIO]) -->
    Hacer1["
      COSTO_PEDIATRIA <-- 2500
      COSTO_MATERNIDAD <-- 3500
      COSTO_OTRO <-- 3000
    "] -->
    Hacer2["
      CATEGORIA_ELEGIDA, como tipo caracter
      DIAS_INTERNACION, como tipo entero
      COSTO_INTERNACION, como tipo entero
    "] -->
    Escribir1[\'Categorías: pediatria, maternidad, otro'/] -->
    Escribir2[\'Ingresar categoría: '/] -->
    Leer1[/CATEGORIA_ELEGIDA/] -->
    Escribir3[\'Ingresar días de internación: '/] -->
    Leer2[/DIAS_INTERNACION/] -->
    Si1{CATEGORIA_ELEGIDA = 'pediatria'}
      Si1 --entonces-->
        Hacer3[COSTO_INTERNACION <-- COSTO_PEDIATRIA * DIAS_INTERNACION] -->
        Escribir4[\'Costo de internación', COSTO_INTERNACION/] -->
        FIN([FIN])
      Si1 --sino-->
        Si2{CATEGORIA_ELEGIDA = 'maternidad'}
          Si2 --entonces-->
            Hacer4[COSTO_INTERNACION <-- COSTO_MATERNIDAD * DIAS_INTERNACION] -->
            Escribir5[\'Costo de internación', COSTO_INTERNACION/] -->
            FIN
          Si2 --sino-->
            Hacer5[COSTO_INTERNACION <-- COSTO_OTRO * DIAS_INTERNACION] -->
            Escribir[\'Costo de internación', COSTO_INTERNACION/] -->
            FIN
```
