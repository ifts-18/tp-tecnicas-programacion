## Consigna:

Se requiere de un algoritmo que calcule la tabla de multiplicar del número que el usuario desee. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***escribir*** "Ingrese qué tabla de multiplicar precisa:"
    - ***leer*** NUMERO
    - ***escribir*** "Tabla de multiplicar del", NUMERO
    - ***para cada*** i en el rango desde 1 hasta 10 (inclusive):
        - ***hacer*** RESULTADO <-- NUMERO * i
        - ***escribir*** NUMERO, "x", i, "=", RESULTADO
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicio --> escribir1[\"'Ingrese qué tabla de multiplicar precisa:'"/]
    escribir1 --> leer1[/NUMERO/]
    leer1 --> escribir2[\"'Tabla de multiplicar del', NUMERO"/]
    escribir2 --> si{i <= 11}
    si --> calcular["RESULTADO <-- NUMERO * i"]
    calcular --> escribir3[\"NUMERO, 'x', i, '=', RESULTADO"/]
    escribir3 --> fin([FIN])
```