"""
Se requiere de un algoritmo que calcule la suma de los 100 números enteros.
"""

suma:int = 0
i = 1
while i <= 100:
    suma += i
    i += 1
print(suma)
