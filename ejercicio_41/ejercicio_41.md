# Consigna:

41. Se requiere de un algoritmo que permita a un runner ingresar la cantidad de kilómetros recorridos por día. Se debe tener en cuenta que el runner entrena al menos 4 días por semana y que cada 2 kilómetros recorrido se pierden 380 kcal. Concluida la carga informar el total de kilómetros recorridos, el promedio semanal y el total de calorías quemadas.

Pseudocodigo

- **Inicio**
    - **Hacer**: 
        - KM_RECORRIDOS <--- 0
        - CALORIAS <--- 0
        - I <--- 0
    - **Mientras**  I < 4 **Repetir**
        - **Escribir**: "Ingrese los kilometros recorridos"
        - **Leer**: KM_RECORRIDOS_INGRESADOS
        - **Hacer**: KW_RECORRIDOS += KM_RECORRIDOS_INGRESADOS
        - **Hacer**: CALORIAS_QUEMADAS <--- KM_RECORRIDOS_INGRESADOS * 380
        - **Hacer**: CALORIAS += CALORIAS_QUEMADAS
        - **Hacer**: I = I + 1
    - **Fin del ciclo**
    - **Hacer**: PROMEDIO_SEMANAL <--- KM_RECORRIDOS / 4
    - **Escribir**:
        - KM_RECORRIDOS
        - PROMEDIO_SEMANAL
        - CALORIAS
- **Fin**