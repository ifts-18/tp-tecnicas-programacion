# Se requiere de un algoritmo que lea un número y calcule la suma de los números que le anteceden al número leído, desde el 0 en adelante. Por ejemplo: si el usuario ingreso el número 5, el algoritmo deberá sumar el número 1, 2, 3, 4 y 5. **DF - PS - PY**

numero = int(input('Ingrese un número: '))
suma = 0

for i in range(numero + 1):
    suma += i

print('La suma de los números anteriores a',numero,'es',suma)