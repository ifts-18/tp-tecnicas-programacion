## Consigna:

Se necesita un algoritmo que permita al usuario ingrese un número 
e informe si el mismo es PAR (ver operador mod).

### Pseudocódigo

- **inicio**
    - ***inicializar*** numero <-- entero
    - ***escribir*** 'Ingrese un número'
    - ***guardar datos en variables*** numero
    - ***evaluar condición*** numero mod 2 = 0
      - ***si es verdad*** 
        - ***escribir*** 'Es par'
      - ***si no***
        - ***escribir*** 'No es par'
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([inicio_diagrama]) --> 
    inicializar["
      numero <-- entero
      "] -->
    escribir[\'Ingrese un número'/] -->
    guardarEnVariable[/"
      numero
      "/] -->
    evaluarCondicion{"
      numero mod 2 = 0
    "}
      evaluarCondicion --> 
        |SI| esVerdadero['Es par']
          --> fin([fin_diagrama])
      evaluarCondicion --> 
        |NO| esFalso['No es par']
          --> fin([fin_diagrama])
```
