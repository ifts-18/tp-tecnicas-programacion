"""
Se requiere un algoritmo que permita ingresar el precio de 4 productos 
y luego permita ingresar la cantidad de productos que se vendieron de 
cada tipo. Finalmente, deberá informar el monto total de la compra.
"""

precio_prod_1: float
precio_prod_2: float
precio_prod_3: float
precio_prod_4: float

cantidad_ventas_prod_1: int
cantidad_ventas_prod_2: int
cantidad_ventas_prod_3: int
cantidad_ventas_prod_4: int

print('Ingrese el precio de los productos')
precio_prod_1 = float(input('Producto 1: '))
precio_prod_2 = float(input('Producto 2: '))
precio_prod_3 = float(input('Producto 3: '))
precio_prod_4 = float(input('Producto 4: '))

print('Ingrese la cantidad de ventas de cada producto')
cantidad_ventas_prod_1 = int(input('Producto 1: '))
cantidad_ventas_prod_2 = int(input('Producto 2: '))
cantidad_ventas_prod_3 = int(input('Producto 3: '))
cantidad_ventas_prod_4 = int(input('Producto 4: '))

total_compra = (precio_prod_1 * cantidad_ventas_prod_1) + \
  (precio_prod_2 * cantidad_ventas_prod_2) + \
  (precio_prod_3 * cantidad_ventas_prod_3) + \
  (precio_prod_4 * cantidad_ventas_prod_4)

print('Total de la compra:', total_compra)
