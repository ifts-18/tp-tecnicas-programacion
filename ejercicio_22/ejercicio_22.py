# Se requiere un algoritmo que calcule el sueldo neto de un trabajador. Para ello, el algoritmo debe admitir el ingreso del monto a cobrar por horas y el total de horas trabajadas. Si el empleado trabajo más de 160 horas mensuales se deben considerar la diferencia como horas extras y el monto por hora deberá ser el doble del valor ingresado en un inicio. DF - PS - PY

monto_por_hora = float(input("Ingrese el monto a cobrar por hora: "))
total_horas_trabajadas = int(input("Ingrese el total de horas trabajadas: "))

horas_extras = max(total_horas_trabajadas - 160, 0)
valor_horas_extra = monto_por_hora * 2

sueldo_normal = monto_por_hora * 160
sueldo_total_extra = sueldo_normal + (valor_horas_extra * horas_extras)

if total_horas_trabajadas > 160:
    print("El sueldo total es: $", sueldo_total_extra)
else:
    print("El sueldo total es: $", sueldo_normal)
