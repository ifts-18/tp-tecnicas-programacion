# Consigna:

26. Se requiere un algoritmo que solicite ingresar:
a. Nombre del Artículo.
b. Precio o costo unitario.
c. Número de departamento en donde se localiza el producto.
En base a los datos ingresados se deberá calcular el incremento de los costos del producto y mostrar el costo final. DF – PS

Pseudocodigo:
- **Inicio**
    - **Hacer** NOMBRE_ARTICULO <--- STRING
    - **Hacer** PRECIO <--- FLOAT
    - **Hacer** DEPARTAMENTO <--- ENTERO 
    - **Escribir** "Ingrese el nombre del producto"
    - **Leer** NOMBRE_ARTICULO
    - **Escribir** "Ingrese su precio"
    - **Leer** PRECIO
    - **Escribir** "Ingrese el departamento del producto"
    - **Leer** DEPARTAMENTO
    - **Hacer** INCREMENTO <-- 0
    - **SI** DEPARTAMENTO = 1
        - **Hacer** INCREMENTO = PRECIO * 0.1
        - **SINO** 
            - **SI** DEPARTAMENTO = 2
                - **Hacer** INCREMENTO = PRECIO * 0.15
                - **SINO**
                        - **Hacer** INCREMENTO = PRECIO *"0.2
    - **Hacer** COSTO_TOTAL <--- PRECIO + INCREMENTO
    - **Escribir** "El costo final del articulo ", NOMBRE_ARTICULO, "es ", COSTO_TOTAL
- **Fin**