# Consigna:

23. Se requiere un algoritmo que calcule la factura de luz eléctrica. Para ello, se debe permitir el ingreso de Kw por mes. Para calcular el costo total se debe considerar que los primeros 30 Kw cuestan $6.03, los siguientes
90 cuestan $6.19, los siguientes 80 Kw $6.78 y los siguientes $7.24. DF – PS - PY

Pseudocodigo

- **Inicio**
    - ***hacer***: 
        - KW_MENSUALES <--- FLOAT
        - COSTO_TOTAL <--- FLOAT
    - ***escribir***: "Ingrese los kw mensuales"
    - ***leer***: KW_MENSUALES
    - ***hacer***: COSTO_TOTAL = 0
    - ***si***: KW_MENSUALES <= 30
        - ***entonces***
            - ***hacer***:COSTO_TOTAL = KW_MENSUALES * 6.03
            - **ESCRIBIR** "El costo de la factura es: ", COSTO_TOTAL
        - ***si no***
            - ***si***: KW_MENSUALES <= 120
                - ***entonces*** 
                    - ***hacer***: COSTO_TOTAL = 30 * 6.03
                    - ***hacer***: KW_RESTANTE <--- KW_MENSUALES - 30
                    - ***SI*** KW_RESTANTE <= 90
                        - **entonces**
                            - **hacer** COSTO_TOTAL = COSTO_TOTAL + (KW_RESTANTES * 6.19)
                            - **ESCRIBIR** "El costo de la factura es: ", COSTO_TOTAL
                        - **si no** 
                            - **si**: KW_RESTANTE <= 170
                                -**entonces**
                                    -**hacer** COSTO_TOTAL = COSTO_TOTAL + (90 * 6.19)
                                    - **HACER** KW_RESTANTE = KW_RESTANTE - 90
                                    - **HACER** COSTO_TOTAL = COSTO_TOTAL + (KW_RESTAMTE * 6.78)
                                    - **ESCRIBIR** "El costo de la factura es: ", COSTO_TOTAL
                                - **SI NO**
                                    -**hacer** COSTO_TOTAL = COSTO_TOTAL + (90 * 6.19)
                                    - **HACER** COSTO_TOTAL = COSTO_TOTAL + (80 * 6.78)
                                    - **HACER** KW_RESTANTE = KW_RESTANTE - 170
                                    - **HACER** COSTO_TOTAL = COSTO_TOTAL + (KR_RESTANTE * 7.24)
                                    - **ESCRIBIR** "El costo de la factura es: ", COSTO_TOTAL
    - **sino**
        - **hacer** COSTO_TOTAL = 30 * 6.03
        - **hacer** COSTO_TOTAL = COSTO_TOTAL + (90 * 6.19)
        - **hacer** COSTO_TOTAL = COSTO_TOTAL + (80 * 6.78)
        - **hacer** KW_RESTANTE = KW_MENSUALES - 200
        - **hacer** COSTO_TOTAL = COSTO_TOTAL + (KW_RESTANTE * 7.24)
        - **ESCRIBIR** "El costo de la factura es: ", COSTO_TOTAL


- **Fin**
