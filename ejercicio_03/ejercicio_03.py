"""
Escribir un algoritmo que permita al usuario calcular una
potencia y luego muestre el resultado de este cálculo. Para 
ello el usuario debe ingresar un número y luego su exponente. 
Es decir: ejemplo: 5^2 = 25 (5 elevado al cuadrado, es igual 
a 25).
"""

numero_base: int
numero_exponente: int
print('Ingrese un número y un exponente para calcular su potencia')
numero_base = int(input('Base: '))
numero_exponente = int(input('Exponente: '))
resultado_potencia = numero_base ** numero_exponente
print('Resultado:', resultado_potencia)
