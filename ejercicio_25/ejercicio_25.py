# Se requiere un algoritmo que solicite ingresar:

# - ***a.*** Importe de ventas de refacciones.

# - ***b.*** Importe de ventas de servicio.

# - ***c.*** Importe de ventas de autos y camiones.

# Calcule el importe TOTAL sumando los tres importes anteriores y el promedio de ventas (TOTAL / 3) y muestre al usuario los resultados de este cálculo. Si el promedio de ventas es mayor o igual a $50.000 deberá mostrar el mensaje: “Se alcanzó el objetivo” de lo contrario deberá mostrar el mensaje “Buscar nuevas estrategias de ventas”. **DF - PS - PY**

importe_ventas_refacciones = float(input('Ingrese el importe de ventas de refacciones: '))
importe_ventas_servicio = float(input('Ingrese el importe de ventas de servicio: '))
importe_ventas_autos_camiones = float(input('Ingrese el importe de ventas de autos y camiones: '))

importe_total_ventas = importe_ventas_refacciones + importe_ventas_servicio + importe_ventas_autos_camiones

promedio_ventas = importe_total_ventas / 3

if(promedio_ventas >= 50000):
    print('Se alcanzó el objetivo')
else:
    print('Buscar nuevas estrategias de ventas')