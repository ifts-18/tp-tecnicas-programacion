"""
Escribir un algoritmo que intercambie dos valores 
ingresados por el usuario. Ejemplo: Si la variable num1 
al inicio del algoritmo vale 5 y la variable num2 vale 10, 
al final del algoritmo num1 debe valer 10 y num2 5.
"""

print('Ingrese dos números enteros:')
numero_1 = int(input('Número 1: '))
numero_2 = int(input('Número 2: '))
intercambio = numero_2
numero_2 = numero_1
numero_1 = intercambio
print('Número 1:', numero_1)
print('Número 2:', numero_2)
