# Se requiere de un algoritmo que permita el ingreso de las edades de un grupo de personas. Una vez cargados todos estos valores informar el promedio de sus edades.**DF - PS - PY**

cantidad_personas = int(input("Ingrese la cantidad de personas: "))
edades = []

for i in range(cantidad_personas):
    edad = int(input("Ingrese la edad de la persona {}: ".format(i + 1)))
    edades += [edad]

promedio = sum(edades) / cantidad_personas

print("El promedio de edades es:", promedio)
