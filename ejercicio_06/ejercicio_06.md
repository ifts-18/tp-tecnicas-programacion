## Consigna:

Escribir un algoritmo que permita al usuario ingresar 2 números y luego le indique el resultado de la división de esos dos números.

### Pseudocódigo

- **inicio**
    - ***inicializar*** dividendo, divisor <-- enteros
    - ***escribir*** 'Ingrese un dividendo y un divisor para calcular la división'
    - ***guardar datos en variables*** dividendo, divisor
    - ***hacer*** resultado_division <-- dividendo / divisor
    - ***escribir*** 'Resultado:', resultado_division
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([inicio_diagrama]) --> inicializar[dividendo, divisor <-- enteros]
    inicializar --> escribirMsje[\'Ingrese un dividendo y un divisor para calcular la división'/]
    escribirMsje --> guardarDatosEnVariables[/dividendo, divisor/]
    guardarDatosEnVariables --> hacerResultado[resultado_division <-- dividendo ** divisor]
    hacerResultado --> escribirResultado[\resultado_division/]
    escribirResultado --> fin([fin_diagrama])
```
