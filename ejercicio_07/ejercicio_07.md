## Consigna:

7. Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee). **DF - PS**

### Pseudocódigo

- **inicio**
    - ***escribir*** 'Ingrese marca y modelo del auto'
    - ***leer*** MARCA
    - ***leer*** MODELO
    - ***escribir*** 'El modelo del auto es:', MODELO, 'y la marca es:', MARCA
- **fin**

```mermaid
flowchart TD
    INICIO([INICIO]) --> ESCRIBIR_CONSULTA_MARCA[\'Ingrese la marca del auto'/]
    ESCRIBIR_CONSULTA_MARCA --> MARCA[/"MARCA"/]
    MARCA --> ESCRIBIR_CONSULTA_MODELO[\'Ingrese el modelo del auto'/]
    ESCRIBIR_CONSULTA_MODELO --> MODELO[/"MODELO"/]
    MODELO --> ESCRIBIR_RESULTADO[\'El modelo del auto es:', MODELO, 'y la marca es:', MARCA/]
    ESCRIBIR_RESULTADO --> FIN([FIN])
```