## Consigna:

Escribir un algoritmo que permita al usuario ingresar 2 números y luego le indique el resultado de la suma de esos dos números.

### Pseudocódigo

- **inicio**
    - ***inicializar*** numero_1, numero_2 <-- enteros
    - ***escribir*** 'Ingrese dos números enteros'
    - ***guardar datos en variables*** numero_1, numero_2
    - ***hacer*** resultado_suma <-- numero_1 + numero_2
    - ***escribir*** resultado_suma
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([inicio_diagrama]) --> inicializar[numero_1, numero_2 <-- enteros]
    inicializar --> escribirMsje[\'Ingrese dos números enteros'/]
    escribirMsje --> guardarDatosEnVariables[/numero_1, numero_2/]
    guardarDatosEnVariables --> hacerResultado[resultado_suma <-- numero_1 + numero_2]
    hacerResultado --> escribirResultado[\resultado_suma/]
    escribirResultado --> fin([fin_diagrama])
```
