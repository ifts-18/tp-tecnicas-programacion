# Se requiere de un algoritmo que permita al gerente de una empresa ingresar el sueldo que posee actualmente un empleado. En caso de que este sea menor a 50.000 se le deberá aumentar un 15% e informar el monto final que obtendrá. En caso de que sea superior a 50.000 se le deberá aumentar un 10% e informar el monto final que obtendrá.
#Cuando el gerente termine de ingresar los sueldos y calcular los aumentos, informará el dinero total que la empresa deberá destinar a dichos aumentos y el monto total que deberán depositar en concepto de sueldos. DF - PS - PY

total_aumentos = 0
total_sueldos = 0

while True:
    sueldo_actual = float(input("Ingrese el sueldo actual del empleado (ingrese 0 para finalizar): "))
    
    if sueldo_actual == 0:
        break
    
    if sueldo_actual < 50000:
        aumento = sueldo_actual * 0.15
        sueldo_final = sueldo_actual + aumento
    else:
        aumento = sueldo_actual * 0.1
        sueldo_final = sueldo_actual + aumento
    
    total_aumentos += aumento
    total_sueldos += sueldo_final

    print("Monto final del sueldo: $", sueldo_final)
    print()
    
print("El dinero total destinado a aumentos es: $", total_aumentos)
print("El monto total de sueldos a depositar es: $", total_sueldos)
