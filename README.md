# TÉCNICAS DE PROGRAMACIÓN
# Trabajo Práctico


## Asignación de ejercicios:

- **Mati**: 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48

- **Sebas**: 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49

- **Cami**: 5-, 8-, 11-, 14-, 17-, 20-, 23-, 26-, 29-, 32, 35-, 38-, 41-, 44-, 47-, 50

# Ya en el doc (completos):

1, 2, 3, 4, 5, 6, 11, 12, 14, 15, 17, 20, 23, 24, 26, 27, 29, 35,38, 41, 44, 47

## Mermaid

Como herramienta para la construcción de los ***diagramas de flujo*** se utiliza [Mermaid](https://mermaid.js.org/syntax/flowchart.html).

[Cheat sheet](https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/)


#### Direcciones posibles para las flechas:

- **TB** - top bottom

- **BT** - bottom top

- **RL** - right left

- **LR** - left right

- **TD** - igual que **TB**


### Símbolos utilizados en los Diagramas de Flujo

Para asignar una expresión o valor a una variable, se utiliza un ***BLOQUE DE ASIGNACIÓN***:

```
Variable <-- expresión o valor
```

---

Símbolo para ***INICIO*** y ***FIN*** del diagrama:

```mermaid
flowchart TD
    inicio([INICIO]) --> fin([FIN])
```

---

Símbolo para expresar ***LECTURA***:

- Utilizado para introducir los datos de entrada:

```mermaid
flowchart LR
    lectura[/"`
        guardar datos en variables
    `"/]
```

---

Símbolo para representar un ***PROCESO*** o ***HACER***:

- En su interior se expresan asignaciones, operaciones aritméticas, cambios de valor de celdas en memoria, etc.

```mermaid
flowchart LR
    proceso["`
        inicializar variables,
        constantes,
        hacer un proceso
    `"]
```

---

Símbolo para expresar ***ESCRITURA***:

- Utilizado para representar la impresión de un resultado.

```mermaid
flowchart LR
    escritura[\"`
        instrucciones al usuario,
        ingresar datos,
        mostrar resultado
    `"/]
```

---

Símbolo para la estructura selectiva ***SI ENTONCES***:

- Utilizado para representar una decisión. En su interior se almacena una condición,
y dependiendo del resultado de la evaluación de la misma se sigue por una de las ramas o caminos alternativos.
- también utilizado como símbolo en las estructuras repetitivas ***REPETIR*** 
y ***MIENTRAS***.

```mermaid
flowchart TD
    siEntonces{"`
        evalúa la
        condición
    `"}
    siEntonces -->|SI| C[UNA COSA]
    siEntonces -->|NO| D[OTRA COSA]
```

---

**Para la estructura selectiva *SI MULTIPLE* se utiliza el símbolo:**
Símbolo para la estructura selectiva ***SI MULTIPLE***:

- Utilizado para representar una decisión múltiple. En su interior se almacena un 
selector, y dependiendo del valor de dicho selector se sigue por una de las ramas
o caminos alternativos.

```mermaid
flowchart TD
    siMultiple{{SELECTOR}}
    siMultiple --> caso_1[Caso 1]
    siMultiple --> caso_2[Caso 2]
    siMultiple --> caso_3[Caso 3]
    siMultiple --> caso_4[Caso 4]

```

---

Símbolo para expresar conexión dentro de una misma página:

```mermaid
flowchart TD
    circulo(((A)))
```

---

Símbolo para expresar conexión entre páginas diferentes:

```mermaid
flowchart TD
    simboloRaro[/A\]
```

---

## Python

Según la guía de estilo de Python, los nombres de variables (y de funciones) deben escribirse en minúscula, con las palabras separadas por un guión bajo para mejorar la legibilidad. Es decir, la recomendación es usar `snake_case`.

`nombre_variable`

https://peps.python.org/pep-0008/#function-and-variable-names

Los nombres de constantes deben escribirse en MAYÚSCULA separadas por un guión bajo.

`NOMBRE_CONSTANTE`

## Consignas particulares para el desarrollo del TP

**Modalidad del trabajo:** grupal hasta 3 integrantes.

**Fecha de entrega:** 19 de Junio de 2023

**Condición de aprobación:** presentar resuelto el 80% de los ejercicios.

**Forma de presentación de los ejercicios:**

- Los ejercicios deben presentarse resueltos con el correspondiente ***diagrama de flujo* (DF)** (respetando las reglas de
creación de los mismos), ***pseudocódigo* (PS)** o ***código en Python* (PY)** según se especifique en la consigna del mismo.

- Los diagramas de flujo pueden realizarse con:
    - Las herramientas vistas en clase
    - Cualquier editor que permita generar el gráfico para volcarlo al documento
    - De forma manual subido como imagen
- El pseudocódigo debe estar escrito en el documento
- El código de Python puede ser presentado en una imagen (no es de carácter obligatorio)

**Aspectos formales de presentación del trabajo:**

- El trabajo deberá estar encabezado por los siguientes datos:
    - Apellidos y nombres
    - Correcto electrónico
    - Se deberá enviar al siguiente correo antes de las 00 hs del día 20 de junio de 2023: pabloa.perez@bue.edu.ar
    - Trabajo entregado fuera de termino estará sujeto a aprobación del mismo

**Formato final de entrega:** PDF

## Guía de ejercicios

***1.*** Escribir un algoritmo que permita al usuario ingresar 2 números y luego le indique el resultado de la suma
de esos dos números. **DF - PS - PY**

***2.*** Escribir un algoritmo que intercambie dos valores ingresados por el usuario. Ejemplo: Si la variable num1 al inicio del algoritmo vale 5 y la variable num2 vale 10, al final del algoritmo num1 debe valer 10 y num2 5. **DF - PS**

***3.*** Escribir un algoritmo que permita al usuario calcular una potencia y luego muestre el resultado de este cálculo. Para ello el usuario debe ingresar un número y luego su exponente. Es decir. Ejemplo: 52 = 25 (5 elevado al cuadrado, es igual a 25). **DF - PS**

***4.*** Escribir un algoritmo que permita al usuario ingresar dos números (el 1er número es el radicando, y el 2do número es el índice) para calcular la raíz y luego le muestre al usuario el resultado de este cálculo. Ejemplo: √25 = 5 (raíz cuadrada de 25, es igual a 5, se deberá permitir al usuario ingresar el 25 y el 2 que actuará de
indice). **DF - PS**

***5.*** Escribir un algoritmo que permita al usuario ingresar 2 números y luego le indique el resultado de la multiplicación entre esos dos números. **DF - PS - PY**

***6.*** Escribir un algoritmo que permita al usuario ingresar 2 números y luego le indique el resultado de la división de esos dos números. **DF - PS**

***7.*** Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee). **DF - PS**

***8.*** Realizar la prueba de escritorio correspondiente al ejercicio 3. ¿En qué líneas se presentan cambios en los valores de las variables? ¿Porqué?

***9.*** Realizar la prueba de escritorio correspondiente al ejercicio 4. ¿En qué líneas se presentan cambios en los valores de las variables? ¿Porqué?

***10.*** Se necesita de un algoritmo que calcule la cantidad de dinero que deberá pagar un cliente, teniendo en cuenta el monto total de la compra y que la tienda ofrece un descuento del 15%. **DF - PS - PY**

***11.*** Se requiere un algoritmo que sume el valor de 5 productos ingresados por un cajero. Una vez calculado el total, se descuente el porcentaje que el cajero ingrese, es decir, se deberá leer el porcentaje de descuento. **DF - PS**

***12.*** Se requiere un algoritmo que permita ingresar el precio de 4 productos y luego permita ingresar la cantidad de productos que se vendieron de cada tipo. Finalmente, deberá informar el monto total de la compra. **DF - PS**

***13.*** Se requiere un algoritmo que calcule el monto a abonar por el servicio de luz eléctrica. Para ello, se deberá ingresar el valor anterior del medidor y el valor actual seguidamente se deberá calcular el total de la factura mensual, considerando que cada Kw cuesta $125.60. **DF - PS - PY**

***14.*** Se requiere un algoritmo que calcule el sueldo neto de un trabajador. Para ello, el algoritmo debe admitir el ingreso del monto a cobrar por horas, el total de horas trabajadas y a este total le debe restar las cargas sociales y aportes (17%). **DF - PS**

***15.*** Se necesita un algoritmo que permita al usuario ingrese un número e informe si el mismo es PAR (ver operador mod). **DF - PS - PY**

***16.*** Se necesita un algoritmo que permita al usuario el ingreso de dos números e informe al usuario si el primer número ingresado es mayor al segundo. **DF - PS - PY**

***17.*** Se necesita un algoritmo que permita al usuario ingresar un número, y en caso de que este sea impar, se informe el resultado de ese número multiplicado por 2. **DF - PS - PY**

***18.*** Se necesita un algoritmo que permita al usuario ingrese un número e informe si el mismo es PAR o IMPAR (ver operador mod). **DF - PS - PY**

***19.*** Se necesita un algoritmo que permita al usuario el ingreso de dos números e informe al usuario si el primer número ingresado es mayor al segundo, si la situación es al revés o si son iguales. **DF - PS - PY**

***20.*** Se requiere de un algoritmo que permita al usuario ingresar 3 números distintos e indique cuál de ellos es el mayor. **DF - PS - PY**

***21.*** Se necesita de un algoritmo que calcule la cantidad de dinero que deberá pagar un cliente, teniendo en cuenta el monto total de la compra. Si el monto de la compra es superior a los $5.000 y el cliente paga en efectivo se le debe aplicar un descuento del 15%, si la compra es mayor a $5.000 y paga con tarjeta se le debe aplicar un descuento del 10%, si la compra es mayor a $2.000 se le debe aplicar un descuento del 10% y si es menor no posee descuento. **DF - PS - PY**

***22.*** Se requiere un algoritmo que calcule el sueldo neto de un trabajador. Para ello, el algoritmo debe admitir el ingreso del monto a cobrar por horas y el total de horas trabajadas. Si el empleado trabajo más de 160 horas mensuales se deben considerar la diferencia como horas extras y el monto por hora deberá ser el doble del valor ingresado en un inicio. **DF - PS - PY**

***23.*** Se requiere un algoritmo que calcule la factura de luz eléctrica. Para ello, se debe permitir el ingreso de Kw por mes. Para calcular el costo total se debe considerar que los primeros 30 Kw cuestan $6.03, los siguientes 90 cuestan $6.19, los siguientes 80 Kw $6.78 y los siguientes $7.24. **DF - PS - PY**

***24.*** Se requiere un algoritmo que calcule el costo de internación de un paciente. Para ello, se debe solicitar ingrese en que categoría se encuentra y la cantidad de días que se encuentra hospitalizado. Las categorías de internación son las siguientes:

- ***a.*** Pediatría

- ***b.*** Maternidad

- ***c.*** Otro

Y los costros de internación por especialidad son los siguientes:

- ***d.*** Pediatría: $2.500

- ***e.*** Maternidad: $3.500

- ***f.*** Otro: $3.000

**DF - PS - PY**

***25.*** Se requiere un algoritmo que solicite ingresar:

- ***a.*** Importe de ventas de refacciones.

- ***b.*** Importe de ventas de servicio.

- ***c.*** Importe de ventas de autos y camiones.

Calcule el importe TOTAL sumando los tres importes anteriores y el promedio de ventas (TOTAL / 3) y muestre al usuario los resultados de este cálculo. Si el promedio de ventas es mayor o igual a $50.000 deberá mostrar el mensaje: “Se alcanzó el objetivo” de lo contrario deberá mostrar el mensaje “Buscar nuevas estrategias de ventas”. **DF - PS - PY**

***26.*** Se requiere un algoritmo que solicite ingresar:

- ***a.*** Nombre del Artículo.

- ***b.*** Precio o costo unitario.

- ***c.*** Número de departamento en donde se localiza el producto.

En base a los datos ingresados se deberá calcular el incremento de los costos del producto y mostrar el costo final. **DF - PS**

| Departamento | Porcentaje de Incremento |
| ------------ | -------------------------|
| 1            | 10%                      |
| 2            | 15%                      |
| 3            | 20%                      |
| Otro         |  5%                      |

**Estructuras Repetitivas**

***27.*** Se requiere de un algoritmo que calcule la suma de los 100 números enteros. **DF - PS**

***28.*** Se requiere de un algoritmo que lea un número y calcule la suma de los números que le anteceden al número leído, desde el 0 en adelante. Por ejemplo: si el usuario ingreso el número 5, el algoritmo deberá sumar el número 1, 2, 3, 4 y 5. **DF - PS - PY**

***29.*** Se requiere de un algoritmo que calcule la suma de los números pares desde el 1 al 200 (operador modulo). **DF - PS**

***30.*** Se requiere de un algoritmo que lea la serie de números positivos hasta que el usuario ingrese un CERO. De los números ingresados informar cual fue el mayor y el menor número ingresado. **DF - PS**

***31.*** Se requiere de un algoritmo que calcule la tabla de multiplicar del número que el usuario desee. **DF - PS - PY**

***32.*** Se requiere de un algoritmo que lea la serie de números positivos hasta que el usuario ingrese un CERO. Cuando el usuario ingrese el valor determinado para parar con el ingreso de valores informará: la sumatoria de los valores ingresados, la cantidad de números leídos, el mayor valor ingresado y el menor. **DF - PS**

***33.*** Se requiere de un algoritmo que permita el ingreso de 10 notas, una por cada alumno y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores. **DF - PS**

***34.*** Se requiere de un algoritmo que permita el ingreso de las edades de un grupo de personas. Una vez cargados todos estos valores informar el promedio de sus edades. **DF - PS - PY**

***35.*** Se requiere de un algoritmo que permita el ingreso de valores enteros. Una vez cargados todos los valores que considere el usuario informar cuántos de ellos son pares y cuantos impares. **DF - PS**

***36.*** Se requiere de un algoritmo que permita ingresar el sueldo de 10 empleados de una empresa, una vez registrados estos valores se informe el monto total que la empresa destina en sueldos por mes, el mayor sueldo registrado y el menor de ellos. **DF - PS**

***37.*** Se requiere de un algoritmo que permita ingresar los registros de un censo hasta que el censista indique que ya ha cargado todos los resultados. Cuando esto ocurra, se deberá informar cantidad de personas censadas, el promedio de edad de ellos y cuantas de estas personas cuentan con internet en sus hogares. **DF - PS - PY**

***38.*** Se requiere de un algoritmo que permita ingresar las ventas realizadas por cada empleado por día. Los empleados de la empresa son: María y Pablo. Una vez ingresados todas las ventas de cada empleado, informar la comisión que les corresponderá por dichas ventas. La comisión se corresponderá al 15% del total vendido. De ser posible, informar la mayor venta registrada en el día y la menor. **DF - PS**

***39.*** Se requiere de un algoritmo que permita calcular el porcentaje de aprobados y desaprobados de los alumnos en un curso, teniendo en cuenta que la nota mínima para aprobar es un 6. **DF - PS**

***40.*** Se requiere de un algoritmo que permita al gerente de una empresa ingresar el sueldo que posee actualmente un empleado. En caso de que este sea menor a 50.000 se le deberá aumentar un 15% e informar el monto final que obtendrá. En caso de que sea superior a 50.000 se le deberá aumentar un 10% e informar el monto final que obtendrá.
Cuando el gerente termine de ingresar los sueldos y calcular los aumentos, informará el dinero total que la empresa deberá destinar a dichos aumentos y el monto total que deberán depositar en concepto de sueldos. **DF - PS - PY**

***41.*** Se requiere de un algoritmo que permita a un runner ingresar la cantidad de kilómetros recorridos por día.
Se debe tener en cuenta que el runner entrena al menos 4 días por semana y quecada 2 kilómetros recorrido se pierden 380 kcal. Concluida la carga informar el total de kilómetros recorridos, el promedio semanal y el total de calorías quemadas. **DF - PS**

***42.*** En una tienda de descuento las personas que van a pagar el importe de su compra llegan a la caja y sacan una bolita de color, que les dirá que descuento tendrán sobre el total de su compra. “Determinar la cantidad que pagara cada cliente desde que la tienda abre hasta que cierra”. Se sabe que si el color de la bolita es rojo el cliente obtendrá un 40% de descuento; si es amarilla un 25% y si es blanca no obtendrá descuento.
(puede utilizar la función Aleatorio que devuelve un numero en un rango definido, por ejemplo del 1 al 3. Ejemplo: Aleatorio (1,3) así defino la función para el seudocódigo). **DF - PS**

***43.*** En un teatro se otorgan descuentos según la edad del cliente. Determinar la cantidad de dinero que el teatro deja de percibir por cada una de las categorías. Tener en cuenta que los niños menores de 5 años no pueden entrar al teatro y que existe un precio único en los asientos. Los descuentos se hacen tomando en cuenta el siguiente cuadro: **DF - PS - PY**

| Categoría |      Edad      | Descuento |
| --------- | -------------- | --------- |
| 1         |   5-14         | 35%       |
| 2         |  15-24         | 25%       |
| 3         |  5-14          | 20%       |
| 4         | 45 en adelante | 15%       |

***44.*** Cierta universidad tiene N estudiantes. Elabore un algoritmo que encuentre el promedio de edad de los estudiantes mayores de 21 años y el promedio de edad del resto de estudiantes. Por cada estudiante se tiene un registro que contiene su código y edad. **DF - PS**

***45.*** Por medio de un algoritmo, determinar la cantidad semanal de dinero que recibirá cada uno de los n obreros de una empresa. Se sabe que cuando las horas que trabajo un obrero exceden de 40, el resto se convierte en horas extras que se pagan al doble de una hora normal. Cuando las horas extras exceden de 8 se pagan las primeras 8 al doble de lo que se paga por una hora normal y el resto al triple. Se debe tener en cuenta que el valor de una hora normal es de $500. **DF - PS**

***46.*** Se requiere de un algoritmo que lea los 250.000 votos de los 3 candidatos a intendente de la ciudad. Luego informar el dato del candidato ganador y los porcentajes de este candidato y los demás. (Función Aleatorio devuelve un numero en un rango definido, en este caso del 1 al 3, Ejemplo: Aleatorio(1,3) así defino la función para el seudocódigo). **DF - PS - PY**

***47*** Se tienen 3 arreglos (desordenados) notas1, notas2 y prom, cada arreglo tiene un tamaño de 100 componentes. Se sabe que la cantidad de alumnos que han rendido los dos parciales de la materia son 83.
Los índices de los arreglos identifican a un alumno a la vez, por ejemplo: notas1[34] y notas2[34] son el mismo alumno. Se requiere que en el arreglo prom se inserte (asignación) el promedio de las dos notas de cada alumno, y que luego el profesor pueda ver por pantalla el promedio de un alumno. **PS - PY**
Por ejemplo: prom[50]

***48*** Teniendo en cuenta las estructuras del ejercicio anterior, inserte las notas de un alumno (faltó a los 2 parciales) que fue a recuperatorio. Solamente hacer la lógica para un solo arreglo. **PS - PY**

***49*** Teniendo en cuenta las estructuras del ejercicio 47, hay que eliminar a un alumno que reprobó la cursada. Solamente hacer la lógica para un sólo arreglo. **PS - PY**

***50*** Teniendo en cuenta las estructuras del ejercicio 47, se debe modificar la nota de un alumno que fue mal cargada. Solamente hacer la lógica para un sólo arreglo. **PS - PY**
