
kw_mensuales = float(input("Ingrese los kw mensuales: "))
costo_total = 0

if kw_mensuales <= 30:
    costo_total = kw_mensuales * 6.03
elif kw_mensuales <= 120:
    costo_total = 30 * 6.03
    kw_restante = kw_mensuales - 30
    if kw_restante <= 90:
        costo_total += kw_restante * 6.19
    elif kw_restante <= 170:
        costo_total += 90 * 6.19
        kw_restante -= 90
        costo_total += kw_restante * 6.78
    else:
        costo_total += 90 * 6.19
        costo_total += 80 * 6.78
        kw_restante -= 170
        costo_total += kw_restante * 7.24
else:
    costo_total = 30 * 6.03
    costo_total += 90 * 6.19
    costo_total += 80 * 6.78
    kw_restante = kw_mensuales - 200
    costo_total += kw_restante * 7.24

print('El costo total es de: ', costo_total)


