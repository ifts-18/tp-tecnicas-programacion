# Teniendo en cuenta las estructuras del ejercicio 47, hay que eliminar a un alumno que reprobó la cursada. Solamente hacer la lógica para un sólo arreglo. PS - PY

import random

notas = [[random.randint(1, 10), random.randint(1, 10)] for _ in range(100)]
prom = [0] * 100  # Arreglo de tamaño 100 para almacenar los promedios
cantidad_alumnos = 83

for i in range(cantidad_alumnos):
    promedio = (notas[i][0] + notas[i][1]) / 2
    prom[i] = promedio

indice_alumno_eliminar = int(input("Ingrese el índice del alumno a eliminar: "))
del notas[indice_alumno_eliminar]
del prom[indice_alumno_eliminar]
print("El alumno ha sido eliminado.")

indice_alumno = int(input("Ingrese el índice del alumno a consultar: "))
print("El promedio del alumno", indice_alumno, "es:", prom[indice_alumno])
