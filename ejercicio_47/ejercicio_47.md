## Consigna:

Se tienen 3 arreglos (desordenados) notas1, notas2 y prom, cada arreglo tiene un tamaño de 100 componentes. Se sabe que la cantidad de alumnos que han rendido los dos parciales de la materia son 83.
Los índices de los arreglos identifican a un alumno a la vez, por ejemplo: notas1[34] y notas2[34] son el mismo alumno. Se requiere que en el arreglo prom se inserte (asignación) el promedio de las dos notas de cada alumno, y que luego el profesor pueda ver por pantalla el promedio de un alumno. PS - PY
Por ejemplo: prom[50]

### Pseudocódigo

- **Inicio**
    - **Hacer**: 
        - notas_1 = [0] * 100
        - notas_2 = [0] * 100
        - PROM = [0] * 100
        - I = 1

    - **Repetir** I <= 83
        - **Escribir** "Ingrese la nota del primer parcial del alumno", [i]
        - **Leer** NOTA_1_ALUMNO
        - **Hacer** NOTAS_1[I] = NOTA_1_ALUMNO
        - **Escribir** "Ingrese la nota del segundo parcial del alumno", [i]
        - **Leer** NOTA_2_ALUMNO
        - **Hacer** NOTAS_2[I] = NOTA_2_ALUMNO
        - **Hacer** PROM[I] = (NOTAS_1[I] + NOTAS_2[I] / 2)
        - **Hacer** I = I + 1
    - **Fin del ciclo**
    - **Escribir**: "Ingrese el numero de un alumno para ver su promedio"
    - **Leer**: ALUMNO 
    - **Escribir** "El promedio del alumno ", ALUMNO, "es: ", PROM[ALUMNO]
- **fin**


