#  Se necesita un algoritmo que permita al usuario el ingreso de dos números e informe al usuario si el primer número ingresado es mayor al segundo. DF - PS - PY

primer_numero = float(input("Ingrese el primer numero: "))
segundo_numero = float(input("Ingrese el segundo numero: "))
if(primer_numero > segundo_numero):
    print(str(primer_numero) + " es mayor que " + str(segundo_numero))
else:
    print(str(primer_numero) + " no es mayor que " + str(segundo_numero))