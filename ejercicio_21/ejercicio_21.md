## Consigna:

Se necesita de un algoritmo que calcule la cantidad de dinero que deberá 
pagar un cliente, teniendo en cuenta el monto total de la compra. 
Si el monto de la compra es superior a los $5.000 y el cliente paga en 
efectivo se le debe aplicar un descuento del 15%, si la compra es mayor 
a $5.000 y paga con tarjeta se le debe aplicar un descuento del 10%, si 
la compra es mayor a $2.000 se le debe aplicar un descuento del 10% y si 
es menor no posee descuento.

### Pseudocódigo

- ***INICIO***
1. ***Hacer*** MONTO_COMPRA, como decimal
2. ***Hacer*** TARJETA <-- "no"
3. ***Escribir*** "Ingresar monto de la compra"
4. ***Leer*** MONTO_COMPRA
5. ***Si*** MONTO_COMPRA <= 2000
    - ***entonces***  
      - ***Escribir*** "Monto a pagar", MONTO_COMPRA
    - ***sino***
      - ***Si*** MONTO_COMPRA <= 5000
        - ***entonces***
          - ***Hacer*** MONTO_COMPRA <-- MONTO_COMPRA * 0.9
          - ***Escribir*** "Monto a pagar", MONTO_COMPRA
        - ***sino***
          - ***Escribir*** "Ingresar "si" para pago con tarjeta"
          - ***Leer*** TARJETA
          - ***Si*** TARJETA = "si"
            - ***entonces***
              - ***Hacer*** MONTO_COMPRA <-- MONTO_COMPRA * 0.9
              - ***Escribir*** "Monto a pagar", MONTO_COMPRA
            - ***sino***
              - ***Hacer*** MONTO_COMPRA <-- MONTO_COMPRA * 0.85
              - ***Escribir*** "Monto a pagar", MONTO_COMPRA
- ***FIN***


### Diagrama de Flujo

```mermaid
flowchart TD
    INICIO([INICIO]) -->
    Hacer1[MONTO_COMPRA, como decimal] -->
    Hacer2[TARJETA <-- 'no'] -->
    Escribir1[\'Ingresar monto de la compra'/] -->
    Hacer3[/MONTO_COMPRA/] -->
    Si1{MONTO_COMPRA <= 2000}
      Si1 --entonces--> 
          Escribir2[\'Monto a pagar', MONTO_COMPRA/] --> 
          FIN([FIN])
      Si1 --sino-->
          Si2{MONTO_COMPRA <= 5000}
            Si2 --entonces-->
                Hacer4[MONTO_COMPRA <-- MONTO_COMPRA * 0.9] --> 
                Escribir3[\'Monto a pagar', MONTO_COMPRA/] -->
                FIN
            Si2 --sino-->
                Escribir4[\'Ingresar 'si' para pago con tarjeta'/] -->
                Leer[/TARJETA/] -->
                Si3{TARJETA = 'si'}
                  Si3 --entonces-->
                    Hacer5[MONTO_COMPRA <-- MONTO_COMPRA * 0.9] -->
                    Escribir5[\'Monto a pagar', MONTO_COMPRA/] -->
                    FIN
                  Si3 --sino-->
                    Hacer6[MONTO_COMPRA <-- MONTO_COMPRA * 0.85] -->
                    Escribir6[\Monto a pagar', MONTO_COMPRA/] -->
                    FIN
```
