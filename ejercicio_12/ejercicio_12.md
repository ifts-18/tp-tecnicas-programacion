## Consigna:

Se requiere un algoritmo que permita ingresar el precio de 4 productos 
y luego permita ingresar la cantidad de productos que se vendieron de 
cada tipo. Finalmente, deberá informar el monto total de la compra.

### Pseudocódigo

- **inicio**
    - ***inicializar***  
      precio_prod_1, precio_prod_2,  
      precio_prod_3, precio_prod_4  
      <-- float 
    - ***inicializar***  
      cantidad_ventas_prod_1, cantidad_ventas_prod_2,  
      cantidad_ventas_prod_3, cantidad_ventas_prod_4  
      <-- enteros
    - ***escribir*** 'Ingrese el precio de los productos'
    - ***guardar datos en variables***  
      precio_prod_1, precio_prod_2,  
      precio_prod_3, precio_prod_4
    - ***escribir*** 'Ingrese la cantidad de ventas de cada producto'
    - ***guardar datos en variables***  
      cantidad_ventas_prod_1, cantidad_ventas_prod_2,  
      cantidad_ventas_prod_3, cantidad_ventas_prod_4
    - ***hacer*** total_compra <--  
      (precio_prod_1 * cantidad_ventas_prod_1) +  
      (precio_prod_2 * cantidad_ventas_prod_2) +  
      (precio_prod_3 * cantidad_ventas_prod_3) +  
      (precio_prod_4 * cantidad_ventas_prod_4)
    - ***escribir*** 'Total de la compra:', total_compra
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([inicio_diagrama]) --> 
    inicializarPrecios["
      precio_prod_1, precio_prod_2, 
      precio_prod_3, precio_prod_4 
      <-- float
      "] -->
    inicializarVentas["
      cantidad_ventas_prod_1, cantidad_ventas_prod_2, 
      cantidad_ventas_prod_3, cantidad_ventas_prod_4 
      <-- enteros
      "] -->
    pedirPrecios[\'Ingrese el precio de los productos'/] -->
    guardarPreciosEnVariables[/"
      precio_prod_1, precio_prod_2, 
      precio_prod_3, precio_prod_4
      "/] -->
    pedirVentas[\'Ingrese la cantidad de ventas de cada producto'/] -->
    guardarVentasEnVariables[/"
      cantidad_ventas_prod_1, cantidad_ventas_prod_2, 
      cantidad_ventas_prod_3, cantidad_ventas_prod_4
      "/] -->
    calcularTotalCompra["
      total_compra <-- 
    (precio_prod_1 * cantidad_ventas_prod_1) + 
    (precio_prod_2 * cantidad_ventas_prod_2) + 
    (precio_prod_3 * cantidad_ventas_prod_3) + 
    (precio_prod_4 * cantidad_ventas_prod_4)
      "] -->
    escribirResultado[\'Total de la compra:', resultado_suma/] -->
    fin([fin_diagrama])
```
