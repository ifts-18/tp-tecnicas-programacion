# Consigna:
***17.*** Se necesita un algoritmo que permita al usuario ingresar un número, y en caso de que este sea impar, se informe el resultado de ese número multiplicado por 2. **DF - PS - PY**

## Pseudocodigo:

- **Inicio**
    - ***inicializar***: numero_ingresado <-- entero
    - ***escribir***: "Ingrese un numero entero"
    - ***leer***: numero_ingresado
    - ***evaluar condicion***: numero_ingresado mod 2 != 0
        - ***si es verdad***
            - ***hacer***: numero_multiplicado <-- numero_ingresado * 2
            - ***escribir*** numero_multiplicado
        - ***si no es verdad***
- **Fin**

```mermaid
flowchart TD
    inicio([Inicio]) --> inicializar[numero_ingresado <-- entero]
    inicializar --> input[\"Ingrese un numero entero"/]
    input --> leer[/numero_ingresado/]
    leer --> condicion{numero_ingresado mod 2 != 0}
    condicion --> |SI| verdadero[numero_multiplicado <-- numero_ingresado * 2]
    verdadero --> mostrar[\numero_multiplicado/]
    condicion -- No --> fin([Fin])
    mostrar --> fin([Fin])

```
