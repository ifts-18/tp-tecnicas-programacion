# En un teatro se otorgan descuentos según la edad del cliente. Determinar la cantidad de dinero que el teatro deja de percibir por cada una de las categorías. Tener en cuenta que los niños menores de 5 años no pueden entrar al teatro y que existe un precio único en los asientos. Los descuentos se hacen tomando en cuenta el siguiente cuadro: **DF - PS - PY**

# | Categoría |      Edad      | Descuento |
# | --------- | -------------- | --------- |
# | 1         |   5-14         | 35%       |
# | 2         |  15-24         | 25%       |
# | 3         |  5-14          | 20%       |
# | 4         | 45 en adelante | 15%       |

precio_asiento = 100.0

dinero_perdido_categoria1 = 0.0
dinero_perdido_categoria2 = 0.0
dinero_perdido_categoria3 = 0.0
dinero_perdido_categoria4 = 0.0

cantidad_clientes = int(input("Ingrese la cantidad de clientes: "))

for i in range(1, cantidad_clientes + 1):
    edad_cliente = int(input("Ingrese la edad del cliente {}: ".format(i)))

    if edad_cliente < 5:
        continue

    if edad_cliente >= 5 and edad_cliente <= 14:
        dinero_perdido_categoria1 += precio_asiento * 0.35
    elif edad_cliente >= 15 and edad_cliente <= 24:
        dinero_perdido_categoria2 += precio_asiento * 0.25
    elif edad_cliente >= 25 and edad_cliente <= 44:
        dinero_perdido_categoria3 += precio_asiento * 0.20
    else:
        dinero_perdido_categoria4 += precio_asiento * 0.15

print("Dinero perdido por la categoría 1:", dinero_perdido_categoria1)
print("Dinero perdido por la categoría 2:", dinero_perdido_categoria2)
print("Dinero perdido por la categoría 3:", dinero_perdido_categoria3)
print("Dinero perdido por la categoría 4:", dinero_perdido_categoria4)
