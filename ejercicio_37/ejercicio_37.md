# Consigna:

Se requiere de un algoritmo que permita ingresar los registros de un censo hasta que el censista indique que ya ha cargado todos los resultados. Cuando esto ocurra, se deberá informar cantidad de personas censadas, el promedio de edad de ellos y cuantas de estas personas cuentan con internet en sus hogares. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***hacer*** PERSONAS_CENSADAS, SUMA_EDADES, PERSONAS_CON_INTERNET <-- 0
    - ***mientras*** Verdadero:
        - ***escribir*** "¿Desea ingresar un nuevo registro? (s/n):"
        - ***leer*** RESPUESTA
        - ***si*** RESPUESTA en minúsculas es igual a "n":
            - ***romper el bucle***
        - ***escribir*** "Ingrese la edad de la persona:"
        - ***leer*** EDAD
        - ***hacer*** SUMA_EDADES <-- SUMA_EDADES + EDAD
        - ***hacer*** PERSONAS_CENSADAS
        - ***escribir*** "¿Tiene acceso a Internet en su hogar? (s/n):"
        - ***leer*** TIENE_INTERNET
        - ***si*** TIENE_INTERNET en minúsculas es igual a "s":
            - ***hacer*** PERSONAS_CON_INTERNET
    - ***hacer*** PROMEDIO_EDAD <-- SUMA_EDADES / PERSONAS_CENSADAS
    - ***escribir*** "Cantidad de personas censadas:", PERSONAS_CENSADAS
    - ***escribir*** "Promedio de edad:", PROMEDIO_EDAD
    - ***escribir*** "Cantidad de personas con acceso a Internet:", PERSONAS_CON_INTERNET
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO]) --> inicializar["PERSONAS_CENSADAS, SUMA_EDADES, PERSONAS_CON_INTERNET <-- 0"]
    inicializar --> mientras{"while = TRUE"}
    mientras --> escribir1[\"'¿Desea ingresar un nuevo registro? s/n'"/]
    escribir1 --> leer1["Leer respuesta desde el usuario"]
```