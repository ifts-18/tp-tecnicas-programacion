# Consigna:

50. Teniendo en cuenta las estructuras del ejercicio 47, se debe modificar la nota de un alumno que fue mal cargada. Solamente hacer la lógica para un solo arreglo. PS – PY

Pseudocodigo:

- **Inicio**
    - **Escribir** "Ingrese el numero del alumno a modificar su nota"
    - **Leer** ALUMNO_INGRESADO
    - **Escribir**: "Ingrese la nueva nota"
    - **Leer**: NOTA_INGRESADA
    - **Hacer** NOTAS_1[ALUMNO_INGRESADO] = NOTA_INGRESADA
- **Fin**
