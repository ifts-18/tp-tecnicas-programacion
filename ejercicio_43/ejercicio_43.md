## Consigna:

En un teatro se otorgan descuentos según la edad del cliente. Determinar la cantidad de dinero que el teatro deja de percibir por cada una de las categorías. Tener en cuenta que los niños menores de 5 años no pueden entrar al teatro y que existe un precio único en los asientos. Los descuentos se hacen tomando en cuenta el siguiente cuadro: **DF - PS - PY**

| Categoría |      Edad      | Descuento |
| --------- | -------------- | --------- |
| 1         |   5-14         | 35%       |
| 2         |  15-24         | 25%       |
| 3         |  5-14          | 20%       |
| 4         | 45 en adelante | 15%       |


### Pseudocódigo

- **inicio**
    - ***inicializar*** dinero_perdido_categoria1, dinero_perdido_categoria2, dinero_perdido_categoria3, dinero_perdido_categoria4 <-- 0

    - ***escribir*** "Ingrese la cantidad de clientes:"
    - ***leer*** cantidad_clientes desde el usuario
    - ***para cada*** i en el rango desde 1 hasta cantidad_clientes:
        - ***escribir*** "Ingrese la edad del cliente", i, ":"
        - ***leer*** edad_cliente desde el usuario
        - ***si*** edad_cliente < 5:
            - ***continuar*** (saltar a la siguiente iteración del bucle)
        - ***si*** edad_cliente >= 5 y edad_cliente <= 14:
            - ***incrementar*** dinero_perdido_categoria1 <-- dinero_perdido_categoria1 + (precio_asiento * 0.35)
        - ***sino si*** edad_cliente >= 15 y edad_cliente <= 24:
            - ***incrementar*** dinero_perdido_categoria2 <-- dinero_perdido_categoria2 + (precio_asiento * 0.25)
        - ***sino si*** edad_cliente >= 25 y edad_cliente <= 44:
            - ***incrementar*** dinero_perdido_categoria3 <-- dinero_perdido_categoria3 + (precio_asiento * 0.20)
        - ***sino***:
            - ***incrementar*** dinero_perdido_categoria4 <-- dinero_perdido_categoria4 + (precio_asiento * 0.15)
    - ***escribir*** "Dinero perdido por la categoría 1:", dinero_perdido_categoria1
    - ***escribir*** "Dinero perdido por la categoría 2:", dinero_perdido_categoria2
    - ***escribir*** "Dinero perdido por la categoría 3:", dinero_perdido_categoria3
    - ***escribir*** "Dinero perdido por la categoría 4:", dinero_perdido_categoria4
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    
```