## Consigna:

Se necesita un algoritmo que permita al usuario el ingreso de dos números e informe al usuario si el primer número ingresado es mayor al segundo, si la situación es al revés o si son iguales. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***escribir*** "Ingrese el primer número:"
    - ***leer*** PRIMER_NUMERO
    - ***escribir*** "Ingrese el segundo número:"
    - ***leer*** SEGUNDO_NUMERO
    - ***si*** PRIMER_NUMERO > SEGUNDO_NUMERO:
        - ***escribir*** "El primer número es mayor al segundo número"
    - ***sino si*** PRIMER_NUMERO < SEGUNDO_NUMERO:
        - ***escribir*** "El primer número es menor al segundo número"
    - ***sino***:
        - ***escribir*** "Los dos números son iguales"
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    ESCRIBIR1[\'Ingrese el primer número:'/]
    LECTURA1[/PRIMER_NUMERO/]
    ESCRIBIR2[\'Ingrese el segundo número:'/]
    LECTURA2[/SEGUNDO_NUMERO/]
    SI{PRIMER_NUMERO > SEGUNDO_NUMERO}
    ESCRIBIR_MAYOR[\'El primer número es mayor al segundo número'/]
    SINO_SI{PRIMER_NUMERO < SEGUNDO_NUMERO}
    ESCRIBIR_MENOR[\'El primer número es menor al segundo número'/]
    ESCRIBIR_IGUAL[\'Los dos números son iguales'/]
    fin([FIN])

    inicio --> ESCRIBIR1
    ESCRIBIR1 --> LECTURA1
    LECTURA1 --> ESCRIBIR2
    ESCRIBIR2 --> LECTURA2
    LECTURA2 --> SI
    SI -- SÍ --> ESCRIBIR_MAYOR
    SI -- NO --> SINO_SI
    SINO_SI -- SÍ --> ESCRIBIR_MENOR
    SINO_SI -- NO --> ESCRIBIR_IGUAL
    ESCRIBIR_MAYOR --> fin
    ESCRIBIR_MENOR --> fin
    ESCRIBIR_IGUAL --> fin
```