## Consigna:

Se requiere de un algoritmo que lea un número y calcule la suma de los números que le anteceden al número leído, desde el 0 en adelante. Por ejemplo: si el usuario ingreso el número 5, el algoritmo deberá sumar el número 1, 2, 3, 4 y 5. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***escribir*** "Ingrese un número:"
    - ***leer*** NUMERO
    - ***hacer*** SUMA <-- 0
    - ***para cada*** i en el rango desde 0 hasta NUMERO (inclusive):
        - ***hacer*** SUMA <-- SUMA + i
    - ***escribir*** "La suma de los números anteriores a", NUMERO, "es", SUMA
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicio --> escribir1[\"'Ingrese un número:'"/]
    escribir1 --> leer1[/NUMERO/]
    leer1 --> inicializar[SUMA <-- 0]
    inicializar --> si{for i=0, NUMERO + 1 <-- SUMA += i}
    si --> escribir2[\"La suma de los números anteriores a',NUMERO, 'es',SUMA"/]
    escribir2 --> fin([Fin])
```