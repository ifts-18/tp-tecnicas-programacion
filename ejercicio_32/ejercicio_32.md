# Consigna:

32. Se requiere de un algoritmo que lea la serie de números positivos hasta que el usuario ingrese un CERO. Cuando el usuario ingrese el valor determinado para parar con el ingreso de valores informará: la sumatoria
de los valores ingresados, la cantidad de números leídos, el mayor valor ingresado y el menor. DF – PS

Pseudocodigo:

- **Inicio**
    - **Hacer**: 
        - SUMATORIA <--- 0
        - CANTIDAD <--- 0
        - MAYOR <--- 0
        - MENOR <--- 0
        - NUMERO_INGRESADO <--- ENTERO
    - **Mientras** NUMERO_INGRESADO <> 0 **Repetir**
    - **Escribir**: "Ingrese numeros positivos, para terminar ingrese 0"
    - **Leer** NUMERO
    - **Hacer**: NUMERO_INGRESADO = NUMERO
    - **SI** NUMERO > MAYOR
        - **Hacer** MAYOR = NUMERO
    - **SI** NUMERO < MENOR
        - **Hacer** MENOR = NUMERO
    - **Hacer**: SUMATORIA + = NUMERO
    - **Hacer**: CANTIDAD += 1
    - **Fin del ciclo**
    - **Escribir**:
        - "La sumatoria de los valores es: ", SUMATORIA
        - "La cantidad de numeros ingresados: ", CANTIDAD
        - "El mayor numero es: ", MAYOR
        - "El menor numero es: ", MENOR
- **Fin**