"""
Se requiere un algoritmo que calcule el costo de internación de un 
paciente. Para ello, se debe solicitar ingrese en que categoría se 
encuentra y la cantidad de días que se encuentra hospitalizado. 

Las categorías de internación son las siguientes:
- Pediatría
- Maternidad
- Otro

Y los costros de internación por especialidad son los siguientes:
- Pediatría: $2.500
- Maternidad: $3.500
- Otro: $3.000
"""

COSTO_PEDIATRIA = 2500
COSTO_MATERNIDAD = 3500
COSTO_OTRO = 3000
categoria_elegida: str
dias_internacion: int
costo_internacion: float

print('Categorías: pediatria, maternidad, otro')
categoria_elegida = input('Ingresar categoría: ')
dias_internacion = int(input('Ingresar días de internación: '))
if categoria_elegida == 'pediatria':
    costo_internacion = COSTO_PEDIATRIA * dias_internacion
elif categoria_elegida == 'maternidad':
    costo_internacion = COSTO_MATERNIDAD * dias_internacion
else:
    costo_internacion = COSTO_OTRO * dias_internacion

print('Costo de internación:', costo_internacion)
