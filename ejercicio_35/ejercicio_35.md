# Consignas:

35. Se requiere de un algoritmo que permita el ingreso de valores enteros. Una vez cargados todos los valores que considere el usuario informar cuántos de ellos son pares y cuantos impares. DF – PS

Pseudocodigo:

- **Inicio**
    - **Hacer**: 
        - PARES <--- 0
        - IMPARES <--- 0
    - **Escribir**: "Ingrese enteros, o 0 para terminar"
    - **Mientras**: NUMERO_INGRESADO <> 0 **Repetir**
        - **Escribir**: "Ingrese un valor entero: "
        - **Leer**: NUMERO_INGRESADO
        - **SI** NUMERO_INGRESADO MOD 2 = 0
            - **Entonces**    
                - **Hacer**: PARES = PARES + 1
            - **Sino**
                - **Hacer**: IMPARES = IMPARES + 1
    - **Fin del ciclo**
    - **Escribir** IMPARES, PARES
- **Fin**