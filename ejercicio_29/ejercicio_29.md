# Consigna:

29. Se requiere de un algoritmo que calcule la suma de los números pares desde el 1 al 200 (operador modulo) DF – PS

Pseudocodigo

- **Inicio**
    - **Hacer**: SUMA <--- 0
    - **Hacer**: I <--- 1
    - **Repetir**: I <= 200
        - **SI** I MOD 2 = 0
            - **Entonces** 
                - **Hacer**: SUMA = SUMA + I
                - **Hacer**: I = I + 1
            - **SINO**
                - **Hacer**: I = I + 1
    - **Fin del ciclo**
- **Fin**