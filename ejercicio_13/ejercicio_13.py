valor_anterior = float(input("Ingrese el valor anterior del medidor: "))
valor_actual = float(input("Ingrese el valor actual del medidor: "))

consumo = valor_actual - valor_anterior
costo_por_kw = 125.60
total_factura = consumo * costo_por_kw

print("El monto a abonar por el servicio de luz eléctrica es de: $", total_factura)
