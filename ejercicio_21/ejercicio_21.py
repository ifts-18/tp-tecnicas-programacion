"""
Se necesita de un algoritmo que calcule la cantidad de dinero que deberá 
pagar un cliente, teniendo en cuenta el monto total de la compra. 
Si el monto de la compra es superior a los $5.000 y el cliente paga en 
efectivo se le debe aplicar un descuento del 15%, si la compra es mayor 
a $5.000 y paga con tarjeta se le debe aplicar un descuento del 10%, si 
la compra es mayor a $2.000 se le debe aplicar un descuento del 10% y si 
es menor no posee descuento.
"""

monto_compra = float(input('Monto compra: $ '))
pago_tarjeta: str = 'no'
if monto_compra <= 2000:
    print('Monto a pagar: $', monto_compra)
elif monto_compra <= 5000:
    monto_compra = monto_compra * 0.9
    print('Monto a pagar: $', monto_compra)
else:
    pago_tarjeta = input('Ingresar "si" para pago con tarjeta: ')
    if pago_tarjeta == 'si':
        monto_compra = monto_compra * 0.9
        print('Monto a pagar: $', monto_compra)
    else:
        monto_compra = monto_compra * 0.85
        print('Monto a pagar: $', monto_compra)
