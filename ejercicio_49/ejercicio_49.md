## Consigna:

Teniendo en cuenta las estructuras del ejercicio 47, hay que eliminar a un alumno que reprobó la cursada. Solamente hacer la lógica para un sólo arreglo. PS - PY

### Pseudocódigo

- **inicio**
    - ***Hacer*** NOTAS, PROM <-- arreglos de tamaño 100
    - ***Hacer*** CANTIDAD_ALUMNOS <-- 83
    - ***para cada*** i en el rango desde 0 hasta CANTIDAD_ALUMNOS - 1:
        - ***Hacer*** PROMEDIO <-- (NOTAS[i][0] + NOTAS[i][1]) / 2
        - ***Hacer*** PROM[i] <-- PROMEDIO
    - ***Escribir*** "Ingrese el índice del alumno a eliminar:"
    - ***Leer*** INDICE_ALUMNO
    - ***Eliminar*** el alumno en la posición INDICE_ALUMNO en las listas NOTAS y PROM
    - ***Escribir*** "El alumno ha sido eliminado."
    - ***Escribir*** "Ingrese el índice del alumno a consultar:"
    - ***Leer*** INDICE_ALUMNO
    - ***Escribir*** "El promedio del alumno", INDICE_ALUMNO, "es:", PROM[INDICE_ALUMNO]
- **fin**

