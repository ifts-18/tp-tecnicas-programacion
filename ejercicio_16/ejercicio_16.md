## Consigna:

 Se necesita un algoritmo que permita al usuario el ingreso de dos números e informe al usuario si el primer número ingresado es mayor al segundo. **DF - PS - PY**

 ### Pseudocódigo

 - **inicio**
    - ***escribir*** "Ingrese el primer número:"
    - ***leer*** PRIMER_NUMERO
    - ***escribir*** "Ingrese el segundo número:"
    - ***leer*** SEGUNDO_NUMERO
    - ***si*** PRIMER_NUMERO > SEGUNDO_NUMERO:
        - ***escribir*** PRIMER_NUMERO, "es mayor que", SEGUNDO_NUMERO
    - ***sino***:
        - ***escribir*** PRIMER_NUMERO, "no es mayor que", SEGUNDO_NUMERO
- **fin**

### Diagrama de flujo

```mermaid
flowchart TD
    inicio([INICIO])
    ESCRIBIR1[\"'Ingrese el primer número:'"/]
    LECTURA1[/"PRIMER_NUMERO"/]
    ESCRIBIR2[\"'Ingrese el segundo número:'"/]
    LECTURA2[/"SEGUNDO_NUMERO"/]
    SI{"PRIMER_NUMERO > SEGUNDO_NUMERO"}
    ESCRIBIR_MAYOR[\"PRIMER_NUMERO, 'es mayor que', SEGUNDO_NUMERO"/]
    ESCRIBIR_NO_MAYOR[\"PRIMER_NUMERO, 'no es mayor que', SEGUNDO_NUMERO"/]
    fin([FIN])

    inicio --> ESCRIBIR1
    ESCRIBIR1 --> LECTURA1
    LECTURA1 --> ESCRIBIR2
    ESCRIBIR2 --> LECTURA2
    LECTURA2 --> SI
    SI -- SÍ --> ESCRIBIR_MAYOR
    SI -- NO --> ESCRIBIR_NO_MAYOR
    ESCRIBIR_MAYOR --> fin
    ESCRIBIR_NO_MAYOR --> fin
```