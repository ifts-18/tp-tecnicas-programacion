## Consigna:

Se necesita de un algoritmo que calcule la cantidad de dinero que deberá pagar un cliente, teniendo en
cuenta el monto total de la compra y que la tienda ofrece un descuento del 15%. **DF – PS - PY**

### Pseudocódigo

- **inicio**
    - ***hacer*** MONTO_COMPRA
    - ***escribir***'Ingrese el monto de la compra'
    - ***hacer*** MONTO_COMPRA
    - ***hacer*** DESCUENTO <-- MONTO_COMPRA * 0.15
    - ***hacer*** PRECIO_TOTAL <-- MONTO_COMPRA - DESCUENTO
    - ***hacer*** PRECIO_TOTAL <-- MONTO_COMPRA - DESCUENTO
    - ***escribir*** 'El monto total con descuento es de: ' + PRECIO_TOTAL
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicializar["MONTO_COMPRA"]
    escribirConsulta[\"'Ingrese el monto de la compra'"/]
    leerMonto[/MONTO_COMPRA/]
    calcularDescuento["DESCUENTO <-- MONTO_COMPRA * 0.15"]
    calcularPrecioTotal["PRECIO_TOTAL <-- MONTO_COMPRA - DESCUENTO"]
    escribirResultado[\"'El monto total con descuento es de: ' + precio_total"/]
    fin([FIN])

    inicio --> inicializar
    inicializar --> escribirConsulta
    escribirConsulta --> leerMonto
    leerMonto --> calcularDescuento
    calcularDescuento --> calcularPrecioTotal
    calcularPrecioTotal --> escribirResultado
    escribirResultado --> fin
```

    