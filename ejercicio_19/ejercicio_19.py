# Se necesita un algoritmo que permita al usuario el ingreso de dos números e informe al usuario si el primer número ingresado es mayor al segundo, si la situación es al revés o si son iguales. DF - PS - PY


primer_numero = float(input("Ingrese el primer número: "))
segundo_numero = float(input("Ingrese el segundo número: "))

if primer_numero > segundo_numero:
    print("El primer número es mayor al segundo número")
elif primer_numero < segundo_numero:
    print("El primer número es menor al segundo número")
else:
    print("Los dos números son iguales")
