# Consigna:

Se requiere de un algoritmo que permita al gerente de una empresa ingresar el sueldo que posee actualmente un empleado. En caso de que este sea menor a 50.000 se le deberá aumentar un 15% e informar el monto final que obtendrá. En caso de que sea superior a 50.000 se le deberá aumentar un 10% e informar el monto final que obtendrá.
Cuando el gerente termine de ingresar los sueldos y calcular los aumentos, informará el dinero total que la empresa deberá destinar a dichos aumentos y el monto total que deberán depositar en concepto de sueldos. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***inicializar*** total_aumentos, total_sueldos <-- 0
    - ***repetir hasta que*** el gerente finalice el ingreso de sueldos:
        - ***leer*** sueldo_actual desde el gerente
        - ***si*** sueldo_actual es igual a 0:
            - ***salir del bucle***
        - ***si*** sueldo_actual es menor a 50.000:
            - ***hacer*** aumento <-- sueldo_actual * 0.15
            - ***hacer*** sueldo_final <-- sueldo_actual + aumento
        - ***sino***:
            - ***hacer*** aumento <-- sueldo_actual * 0.10
            - ***hacer*** sueldo_final <-- sueldo_actual + aumento
        - ***acumular*** aumento en total_aumentos
        - ***acumular*** sueldo_final en total_sueldos
        - ***escribir*** "Monto final del sueldo:", sueldo_final
    - ***escribir*** "El dinero total destinado a aumentos es:", total_aumentos
    - ***escribir*** "El monto total de sueldos a depositar es:", total_sueldos
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    
```