## Consigna

Se requiere un algoritmo que solicite ingresar:

- ***a.*** Importe de ventas de refacciones.

- ***b.*** Importe de ventas de servicio.

- ***c.*** Importe de ventas de autos y camiones.

Calcule el importe TOTAL sumando los tres importes anteriores y el promedio de ventas (TOTAL / 3) y muestre al usuario los resultados de este cálculo. Si el promedio de ventas es mayor o igual a $50.000 deberá mostrar el mensaje: “Se alcanzó el objetivo” de lo contrario deberá mostrar el mensaje “Buscar nuevas estrategias de ventas”. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***escribir*** "Ingrese el importe de ventas de refacciones:"
    - ***leer*** IMPORTE_VENTAS_REFACCIONES
    - ***escribir*** "Ingrese el importe de ventas de servicio:"
    - ***leer*** IMPORTE_VENTAS_SERVICIO
    - ***escribir*** "Ingrese el importe de ventas de autos y camiones:"
    - ***leer*** IMPORTE_VENTAS_AUTOS_CAMIONES
    - ***hacer*** IMPORTE_TOTAL_VENTAS <-- IMPORTE_VENTAS_REFACCIONES + IMPORTE_VENTAS_SERVICIO + IMPORTE_VENTAS_AUTOS_CAMIONES
    - ***hacer*** IMPORTE_VENTAS <-- IMPORTE_TOTAL_VENTAS / 3

    - ***si*** IMPORTE_VENTAS >= 50000:
        - ***escribir*** "Se alcanzó el objetivo"
    - ***sino***:
        - ***escribir*** "Buscar nuevas estrategias de ventas"
- **fin**

### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicio --> escribir1[\"'Ingrese el importe de ventas de refacciones:'"/]
    escribir1 --> leer1[/IMPORTE_VENTAS_REFACCIONES/]
    leer1 --> escribir2[\"'Ingrese el importe de ventas de servicio:'"/]
    escribir2 --> leer2[/IMPORTE_VENTAS_SERVICIO/]
    leer2 --> escribir3[\"'Ingrese el importe de ventas de autos y camiones:'"/]
    escribir3 --> leer3[/IMPORTE_VENTAS_AUTOS_CAMIONES/]
    leer3 --> calcular1[IMPORTE_TOTAL_VENTAS <-- IMPORTE_VENTAS_REFACCIONES + IMPORTE_VENTAS_SERVICIO + IMPORTE_VENTAS_AUTOS_CAMIONES]
    calcular1 --> calcular2[PROMEDIO_VENTAS <-- IMPORTE_TOTAL_VENTAS / 3]
    calcular2 --> si{PROMEDIO_VENTAS >= 50000}
    si --si --> escribir4[\"'Se alcanzó el objetivo'"/]
    si -- no --> escribir5[\"'Buscar nuevas estrategias de ventas'"/]
    escribir4 --> fin([FIN])
    escribir5 --> fin([FIN])
```