# Consigna:

***5.*** Escribir un algoritmo que permita al usuario ingresar 2 números y luego le indique el resultado de la multiplicación entre esos dos números. **DF - PS - PY**

**Pseudocodigo:**

- **Inicio**
    - ***inicializar***: numero_1, numero_2 <-- enteros
    - ***escribir***: "Ingrese dos numeros a multiplicar"
    - ***guardar datos en variables***: numero_1, numero_2
    - ***hacer***: resultado_multiplicacion <-- numero_1 * numero_2
    - ***escribir***: resultado_multiplicacion
- **Fin**

## Diagrama de flujo:

```mermaid
flowchart TD
    inicio([Inicio]) --> inicializar[numero_1, numero_2 <-- enteros]
    inicializar --> input[\"Ingrese dos numeros a multiplicar"/]
    input --> numeros_ingresados[/numero_1, numero_2/]
    numeros_ingresados --> resultado_multiplicacion[resultado_multiplicacion <-- numero_1 * numero_2]
    resultado_multiplicacion --> output[\resultado_multiplicacion/]
    output --> finalizacion([Fin])

```

