## Consigna:

Se requiere de un algoritmo que permita el ingreso de las edades de un grupo de personas. Una vez cargados todos estos valores informar el promedio de sus edades. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - **escribir** "Ingrese la cantidad de personas:"
    - **leer** CANTIDAD_PERSONAS
    - **hacer** EDADES como una lista vacía
    - **para cada** i en el rango desde 0 hasta CANTIDAD_PERSONAS - 1:
        - **escribir** "Ingrese la edad de la persona", i + 1, ":"
        - **leer** EDAD
        - **hacer** EDAD a la lista EDADES
    - **hacer** PROMEDIO <-- suma de los elementos de EDADES dividido por CANTIDAD_PERSONAS
    - **escribir** "El promedio de edades es:", PROMEDIO
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicio --> escribir1[\"'Ingrese la cantidad de personas:'"/]
    escribir1 --> leer1[/CANTIDAD_PERSONAS/]
    leer1 --> inicializar["EDADES = ARRE[ ]"]
    inicializar --> si{for i > 0, i < CANTIDAD_PERSONAS - 1}
    si --> escribir2[\"'Ingrese la edad de la persona', i + 1"/]
    escribir2 --> leer2[/EDAD/]
    leer2 --> agregar["EDADES += [EDAD]"]
    agregar --> calcular["PROMEDIO <-- sum(EDADES) / CANTIDAD_PERSONAS"]
    calcular --> escribir3[\"'El promedio de edades es:', promedio"/]
    escribir3 --> fin([FIN])
```