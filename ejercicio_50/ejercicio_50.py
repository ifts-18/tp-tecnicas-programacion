
# - **Inicio**
#     - **Escribir** "Ingrese el numero del alumno a modificar su nota"
#     - **Leer** ALUMNO_INGRESADO
#     - **Escribir**: "Ingrese la nueva nota"
#     - **Leer**: NOTA_INGRESADA
#     - **Hacer** NOTAS_1[ALUMNO_INGRESADO] = NOTA_INGRESADA
# - **Fin**


notas_1 = [0] * 100
notas_2 = [0] * 100
prom = [0] * 100

for i in range(83):
    notas_1[i] = float(input(f"Ingrese la nota del primer parcial para el alumno: {i}"))
    notas_2[i] = float(input(f"Ingrese la nota del segundo parcial para el alumno: {i}"))

for i in range(83):
    prom[i] = (notas_1[i] + notas_2[i]) / 2

alumno = int(input("Ingrese un alumno para ver su promedio "))

print(f"El promedio del alumno {alumno} es: {prom[alumno]}")

alumno_ingresado = int(input("Ingrese el numero del alumno a modificar la nota: "))
nueva_nota = float(input("Ingrese la nueva nota: "))

notas_1[alumno_ingresado] = nueva_nota


