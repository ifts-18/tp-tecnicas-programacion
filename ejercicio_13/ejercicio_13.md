## Consigna:

Se requiere un algoritmo que calcule el monto a abonar por el servicio de luz eléctrica. Para ello, se deberá ingresar el valor anterior del medidor y el valor actual seguidamente se deberá calcular el total de la factura mensual, considerando que cada Kw cuesta $125.60. **DF - PS - PY**

### Pseudocódigo

- **inicio**
    - ***escribir*** "Ingrese el valor anterior del medidor:"
    - ***leer*** VALOR_ANTERIOR
    - ***escribir*** "Ingrese el valor actual del medidor:"
    - ***leer*** VALOR_ACTUAL
    - ***hacer*** CONSUMO <-- VALOR_ACTUAL - VALOR_ANTERIOR
    - ***definir*** COSTO_POR_KW <-- 125.60
    - ***hacer*** TOTAL_FACTURA <-- CONSUMO * COSTO_POR_KW

    - ***escribir*** "El monto a abonar por el servicio de luz eléctrica es de: , TOTAL_FACTURA
- **fin**


### Diagrama de flujo

```mermaid
flowchart TD
    inicio([INICIO])
    ESCRIBIR1[\"'Ingrese el valor anterior del medidor:'"/]
    LECTURA1[/"VALOR_ANTERIOR"/]
    ESCRIBIR2[\"'Ingrese el valor actual del medidor:'"/]
    LECTURA2[/"VALOR_ACTUAL"/]
    CALCULO1["CONSUMO <-- VALOR_ACTUAL - VALOR_ANTERIOR"]
    DEFINIR1["COSTO_POR_KW <-- 125.60"]
    CALCULO2["TOTAL_FACTURA <-- CONSUMO * COSTO_POR_KW"]
    ESCRIBIR_RESULTADO[\"'El monto a abonar por el servicio de luz eléctrica es de: TOTAL_FACTURA"/]
    fin([FIN])

    inicio --> ESCRIBIR1
    ESCRIBIR1 --> LECTURA1
    LECTURA1 --> ESCRIBIR2
    ESCRIBIR2 --> LECTURA2
    LECTURA2 --> CALCULO1
    CALCULO1 --> DEFINIR1
    DEFINIR1 --> CALCULO2
    CALCULO2 --> ESCRIBIR_RESULTADO
    ESCRIBIR_RESULTADO --> fin
```