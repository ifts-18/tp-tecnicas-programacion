## Consigna:

Escribir un algoritmo que permita al usuario calcular una potencia y luego muestre el resultado de este cálculo. Para ello el usuario debe ingresar un número y luego su exponente. Es decir: ejemplo: 5^2 = 25 (5 elevado al cuadrado, es igual a 25).

### Pseudocódigo

- **inicio**
    - ***inicializar*** numero_base, numero_exponente <-- enteros
    - ***escribir*** 'Ingrese un número y un exponente para calcular su potencia'
    - ***guardar datos en variables*** numero_base, numero_exponente
    - ***hacer*** resultado_potencia <-- numero_base ** numero_exponente
    - ***escribir*** resultado_potencia
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([inicio_diagrama]) --> inicializar[numero_base, numero_exponente <-- enteros]
    inicializar --> escribirMsje[\'Ingrese un número y un exponente para calcular su potencia'/]
    escribirMsje --> guardarDatosEnVariables[/numero_base, numero_exponente/]
    guardarDatosEnVariables --> hacerResultado[resultado_potencia <-- numero_base ** numero_exponente]
    hacerResultado --> escribirResultado[\resultado_potencia/]
    escribirResultado --> fin([fin_diagrama])
```
