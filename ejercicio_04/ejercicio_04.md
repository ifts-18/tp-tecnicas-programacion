## Consigna:

Escribir un algoritmo que permita al usuario ingresar dos números (el 1er numero es el radicando y el 2do numero es el indice) para calcular la raiz y luego le muestre al usuario el resultado de este calculo. Ejemplo: √25 = 5 (raiz cuadrada de 25, es igual a 5, se debera permitir al usuario ingresar el 25 y el 2 que actuara de indice) **DF - PS**

### Pseudocódigo

- **inicio**
    - ***hacer*** RADICANDO,INDICE <-- enteros
    - ***escribir*** 'Ingrese un radicando y un indice'
    - ***leer*** RADICANDO,INDICE
    - ***hacer***  RESULTADO_RAIZ <-- RADICANDO ** (1/INDICE)
    - ***escribir*** RESULTADO_RAIZ
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([INICIO])
    inicializar["RADICANDO, INDICE <-- enteros"]
    escribirConsulta[\'Ingrese un radicando y un indice'/]
    leerRadicando[/RADICANDO/]
    leerIndice[/INDICE/]
    hacerResultado["RESULTADO_RAIZ <-- RADICANDO ** (1/INDICE)"]
    escribirResultado[\RESULTADO_RAIZ/]
    fin([FIN])

    inicio --> inicializar
    inicializar --> escribirConsulta
    escribirConsulta --> leerRadicando
    leerRadicando --> leerIndice
    leerIndice --> hacerResultado
    hacerResultado --> escribirResultado
    escribirResultado --> fin
```

