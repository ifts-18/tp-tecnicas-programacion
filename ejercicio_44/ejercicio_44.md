# Consigna:

44. Cierta universidad tiene N estudiantes. Elabore un algoritmo que encuentre el promedio de edad de los estudiantes mayores de 21 años y el promedio de edad del resto de estudiantes. Por cada estudiante se tiene un registro que contiene su código y edad. DF – PS

Pseudocodigo:

- **Inicio**
    - **Hacer**: 
        - EDAD_MAYORES_21 <--- 0
        - TOTAL_MAYORES_21 <--- 0
        - EDAD_MENORES_21 <--- 0
        - TOTAL_MENORES_21 <--- 0
    - **Escribir**: "Ingrese la cantidad de alumnos"
    - **Leer** CANTIDAD_ALUMNOS
    - **Hacer** I <--- 1
    - **Repetir** I <= CANTIDAD_ALUMNOS
        - **Escribir** "Ingrese la edad del alumno"
        - **Leer** EDAD_ALUMNO
        - **SI** EDAD_ALUMNO > 21
            - **Hacer**: EDAD_MAYORES_21 += EDAD_ALUMNO
            - **Hacer**: TOTAL_MAYORES_21 += 1
        - **SINO** 
            - **Hacer**: EDAD_MENORES_21 += EDAD_ALUMNO
            - **Hacer**: TOTAL_MENORES_21 += 1
    - **Fin del ciclo**
    - **Hacer**: PROMEDIO_MAYORES_21 <--- EDAD_MAYORES_21 / MAYORES_21
    - **Hacer**: PROMEDIO_MENORES_21 <--- EDAD_MENORES_21 / MENORES_21
    - **Escribir**:
        - "El promedio de mayores es ", PROMEDIO_MAYORES_21
        - "El promedio de menores es ", PROMEDIO_MENORES_21
- **Fin**