# Consigna:

***14.*** Se requiere un algoritmo que calcule el sueldo neto de un trabajador. Para ello, el algoritmo debe admitir el ingreso del monto a cobrar por horas, el total de horas trabajadas y a este total le debe restar las cargas sociales y aportes (17%). **DF - PS**

**Pseudocodigo:**

- **Inicio**
    - ***escribir:*** "Ingrese el monto a cobrar por horas"
    - ***guardar datos en variables:*** cobro_por_hora 
    - ***escribir:*** "Ingrese las horas trabajadas"
    - ***guardar datos en variables:*** horas_trabajadas
    - ***hacer:*** total_sin_descuento <-- cobro_por_hora * horas_trabajadas 
    - ***hacer:*** porcentaje_descuento <-- 17 * total_sin_descuento / 100 
    - ***hacer:*** total <-- total_sin_descuento - porcentaje_descuento
    - ***escribir:*** total
- **Fin**

```mermaid
flowchart TD
    inicio([Inicio]) --> input[\"Ingrese el monto a cobrar por horas"/]
    input --> cobro_por_horas[/cobro_por_horas/]
    cobro_por_horas --> input_horas[\"Ingrese la cantidad de horas trabajadas"/]
    input_horas --> horas_trabajadas[/horas_trabajadas/]
    horas_trabajadas --> total_sin_descuento[total_sin_descuento <-- cobro_por_horas * horas_trabajadas]
    total_sin_descuento --> porcentaje_descuento[porcentaje_descuento <-- 17 * total_sin_descuento / 100]
    porcentaje_descuento --> total[total <-- total_sin_descuento - porcentaje_descuento]
    total --> output[\total/]
    output --> finalizacion([Fin])
```