# Se requiere de un algoritmo que permita ingresar los registros de un censo hasta que el censista indique que ya ha cargado todos los resultados. Cuando esto ocurra, se deberá informar cantidad de personas censadas, el promedio de edad de ellos y cuantas de estas personas cuentan con internet en sus hogares. DF - PS - PY

personas_censadas = 0
suma_edades = 0
personas_con_internet = 0

while True:
    respuesta = input("¿Desea ingresar un nuevo registro? (s/n): ")
    if respuesta.lower() == "n":
        break
    
    edad = int(input("Ingrese la edad de la persona: "))
    suma_edades += edad
    personas_censadas += 1
    
    tiene_internet = input("¿Tiene acceso a Internet en su hogar? (s/n): ")
    if tiene_internet.lower() == "s":
        personas_con_internet += 1

promedio_edad = suma_edades / personas_censadas

print("Cantidad de personas censadas:", personas_censadas)
print("Promedio de edad:", promedio_edad)
print("Cantidad de personas con acceso a Internet:", personas_con_internet)
