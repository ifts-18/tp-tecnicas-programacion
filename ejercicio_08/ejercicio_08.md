# Consigna: 

8. Realizar la prueba de escritorio correspondiente al ejercicio 3. ¿En qué líneas se presentan cambios en los valores de las variables? ¿Por qué?

|  NUMERO_BASE   | NUMERO_EXPONENTE | RESULTADO  |
|:----------------: |:-------------:|:----------:|
|        3           |       4        |     81       |
|        2           |        7       |     128       |
|         7          |        2       |       49     |
|        5           |        3       |      125      |
|         4          |         4      |      256      |