# Consigna:

20. Se requiere de un algoritmo que permita al usuario ingresar 3 números distintos e indique cuál de ellos es el mayor. DF – PS - PY

Pseudocodigo

- **Inicio**
    - ***hacer***: MAYOR <--- FLOAT
    - ***escribir***: "Ingrese tres numeros"
    - ***leer***: NUMERO_1, NUMERO_2, NUMERO_3
    - ***hacer***: MAYOR = NUMERO_1
    - ***si***: NUMERO_2 > MAYOR
        - ***entonces***
            - ***hacer***: MAYOR = NUMERO_2
            - ***escribir*** "El mayor es: ", MAYOR
        - ***si no***
            - ***si***: NUMERO_3 > MAYOR
                - ***entonces*** 
                    - ***hacer***: MAYOR = NUMERO_3
                    - ***escribir*** "El mayor es: ", MAYOR
                - ***si no*** 
                    - ***escribir*** "El mayor es: ", MAYOR

- **Fin**