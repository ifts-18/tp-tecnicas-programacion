# Consigna:

***11.*** Se requiere un algoritmo que sume el valor de 5 productos ingresados por un cajero. Una vez calculado el total, se descuente el porcentaje que el cajero ingrese, es decir, se deberá leer el porcentaje de descuento. **DF - PS**

**Pseudocodigo:**

- **Inicio**
    - ***inicializar:*** producto_1, producto_2, producto_3, producto_4, producto_5 <-- float
    - ***guardar datos en variables:*** producto_1, producto_2, producto_3, producto_4, producto_5 
    - ***hacer:*** total_sin_descuento <-- producto_1 + producto_2 + producto_3 + producto_4 + producto_5
    - ***escribir:*** "Ingrese el descuento"
    - ***guardar datos en variables:*** descuento 
    - ***hacer:*** porcentaje_descuento <-- total_sin_descuento * descuento / 100
    - ***hacer:*** total <-- total_sin_descuento - porcentaje_descuento
    - ***escribir:*** total
- **Fin**

```mermaid
flowchart TD

    inicio([Inicio]) --> inicializar[producto_1, producto_2, producto_3, producto_4, producto_5 <-- float]
    inicializar --> productos[/producto_1, producto_2, producto_3, producto_4, producto_5/]
    productos --> suma_productos[total_sin_descuento <-- producto_1 + producto_2 + producto_3 + producto_4 + producto_5]
    suma_productos --> input[\"Ingrese el descuento"/]
    input --> descuento[/descuento/]
    descuento --> porcentaje_descuento[porcentaje_descuento <-- total_sin_descuento * descuento / 100]
    porcentaje_descuento --> total[total <-- total_sin_descuento - porcentaje_descuento]
    total --> output[\total/]
    output --> finalizacion([Fin])
    
```