# Consigna:

38. Se requiere de un algoritmo que permita ingresar las ventas realizadas por cada empleado por día. Los empleados de la empresa son: María y Pablo. Una vez ingresados todas las ventas de cada empleado, informar la comisión que les corresponderá por dichas ventas. La comisión se corresponderá al 15% del total vendido. De ser posible, informar la mayor venta registrada en el día y la menor.

Pseudocodigo:

- **Inicio**
    - **Hacer**:
        - VENTAS_MARIA <--- 0
        - VENTAS_PABLO <--- 0
    - **Hacer**:
        - MAYOR_VENTA <--- 0
        - MENOR_VENTA <--- 0
    - **Hacer**:
        - COMISION_MARIA = 0
        - COMISION_PABLO = 0
    - **Escribir** "Ingrese las ventas de los empleados e ingrese 0 para finalizar"
    - **Mientras** INGRESO_VENTAS_MARIA <> 0 **Repetir**
        - **Escribir** "Ingrese las ventas de Maria"
        - **Leer** INGRESO_VENTAS_MARIA 
        - **Hacer** VENTAS_MARIA = VENTAS_MARIA + INGRESO_VENTAS_MARIA
        - **SI** MAYOR_VENTA < INGRESO_VENTAS_MARIA
            - **Hacer** MAYOR_VENTA = INGRESO_VENTAS_MARIA
        - **SI** MENOR_VENTA > INGRESO_VENTAS_MARIA
            - **Hacer** MENOR_VENTA = INGRESO_VENTAS_MARIA
    - **Fin del ciclo**
    - **Mientras** INGRESO_VENTAS_PABLO <> 0 **Repetir**
        - **Escribir** "Ingrese las ventas de Pablo"
        - **Leer** INGRESO_VENTAS_PABLO 
        - **Hacer** VENTAS_PABLO = VENTAS_PABLO + INGRESO_VENTAS_PABLO
        - **SI** MAYOR_VENTA < INGRESO_VENTAS_PABLO
            - **Hacer** MAYOR_VENTA = INGRESO_VENTAS_PABLO
        - **SI** MENOR_VENTA > INGRESO_VENTAS_PABLO
            - **Hacer** MENOR_VENTA = INGRESO_VENTAS_PABLO
    - **Fin del ciclo**
    - **Hacer**
        - COMISION_MARIA = VENTAS_MARIA * 0.15
        - COMISION_PABLO = VENTAS_PABLO * 0.15
    - **Escribir** 
        - "La comision de maria es: ", COMISION_MARIA
        - "La comision de pablo es: ", COMISION_PABLO
        - "La mayor venta del dia fue: ", MAYOR_VENTA
        - "La menor venta del dia fue: ", MENOR_VENTA    
- **Fin**