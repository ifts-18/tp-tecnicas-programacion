# Se requiere de un algoritmo que lea los 250.000 votos de los 3 candidatos a intendente de la ciudad. Luego informar el dato del candidato ganador y los porcentajes de este candidato y los demás. (Función Aleatorio devuelve un numero en un rango definido, en este caso del 1 al 3, Ejemplo: Aleatorio(1,3) así defino la función para el seudocódigo). DF - PS - PY

import random

total_votos_candidato1 = 0
total_votos_candidato2 = 0
total_votos_candidato3 = 0
total_votos = 250000

for voto in range(total_votos):
    numero_candidato = random.randint(1, 3)
    
    if numero_candidato == 1:
        total_votos_candidato1 += 1
    elif numero_candidato == 2:
        total_votos_candidato2 += 1
    else:
        total_votos_candidato3 += 1

porcentaje_candidato1 = (total_votos_candidato1 / total_votos) * 100
porcentaje_candidato2 = (total_votos_candidato2 / total_votos) * 100
porcentaje_candidato3 = (total_votos_candidato3 / total_votos) * 100

if total_votos_candidato1 > total_votos_candidato2 and total_votos_candidato1 > total_votos_candidato3:
    print("El candidato 1 es el ganador con", porcentaje_candidato1, "% de los votos.")
elif total_votos_candidato2 > total_votos_candidato1 and total_votos_candidato2 > total_votos_candidato3:
    print("El candidato 2 es el ganador con", porcentaje_candidato2, "% de los votos.")
else:
    print("El candidato 3 es el ganador con", porcentaje_candidato3, "% de los votos.")

