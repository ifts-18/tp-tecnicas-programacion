## Consigna:

Se requiere de un algoritmo que calcule la suma de los 100 números enteros.


### Pseudocódigo

- ***INICIO***
  - ***Hacer***  
    - SUMA <-- 0
    - I <-- 1
  - ***Mientras*** I <= 100 ***repetir***
    - ***Hacer***  
      - SUMA <-- SUMA + I
      - I <-- I + 1
- ***FIN***


### Diagrama de Flujo

```mermaid
flowchart TD
    INICIO([INICIO]) -->
    Hacer1["
      SUMA <-- 0,
      I <-- 1
    "] --> 
    Mientras{I <= 100}
      Mientras --si-->
        Hacer2["
          SUMA <-- SUMA + 1,
          I <-- I + 1
        "] --> 
      Mientras --no-->
        FIN([FIN])
```
