## Consigna:

Escribir un algoritmo que intercambie dos valores ingresados 
por el usuario. Ejemplo: Si la variable num1 al inicio del algoritmo vale 5 y la variable num2 vale 10, al final del algoritmo num1 
debe valer 10 y num2 5.

### Pseudocódigo

- **inicio**
    - ***escribir*** 'Ingrese dos números enteros'
    - ***leer*** numero_1, numero_2
    - ***hacer*** intercambio <-- numero_2
    - ***hacer*** numero_2 <-- numero_1
    - ***hacer*** numero_1 <-- intercambio
    - ***escribir*** numero_1
    - ***escribir*** numero_2
- **fin**


### Diagrama de Flujo

```mermaid
flowchart TD
    inicio([inicio]) --> escribir[\'Ingrese dos números enteros'/]
    escribir --> leer[/numero_1, numero_2/]
    leer --> intercambio[intercambio <-- numero_2]
    intercambio --> numero_2[numero_2 <-- numero_1]
    numero_2 --> numero_1[numero_1 <-- intercambio]
    numero_1 --> escribirNumero_1[\numero_1/]
    escribirNumero_1 --> escribirNumero_2[\numero_2/]
    escribirNumero_2 --> fin([fin])
```
